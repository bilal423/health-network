<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */


define('DB_NAME', 'health_network');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'admin123');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Wlsi,XZn0u27~W?D`.oQHBG9x=j<c:Y)I}U6@nkZ%(Y7lUed`g@DCo1gx!Z?IYu^');
define('SECURE_AUTH_KEY',  'W2oU4/Z&hi^<-{oZ:xqtj|H1U?(~=Vd=m bE1ov4Dy`MQPd[TNTWDXvBH3V`EjmH');
define('LOGGED_IN_KEY',    'VmPPrCG0.(BzdB,@:.{gKK];vs8/@xv1Xj3=FWH7[>#%DJ70/a:U!R_N)YP/53_y');
define('NONCE_KEY',        'f+5~:EV;*2L-/Hij4Xndh3$<=]x1>a^J2hNV&;:F{Ytm4HX^:VexHsqef:=8f/!2');
define('AUTH_SALT',        '@f,Kd+DVZITg4;tSgs[{4=<q>Gx3cVyO!Qk!/wWhmsXx?MvJ,:v $9ie^J_zII+E');
define('SECURE_AUTH_SALT', 'zY{)}N%1zVn;_}-i}@COr|5,zwAlXdPnGmnczLlaYXcj?v|j>0mc|(/}x|Y1n]_[');
define('LOGGED_IN_SALT',   '%%@2g[+Vxg2)f;+Q.qv f[ua1D*NO~(c{^%,$TK4OMuv|;=7&c$^?Jd`co1|S^B7');
define('NONCE_SALT',       'PL{SX`|DgF}8T6L/|eC-/TurU}H;L6B=+})rld@C.uIYdsvVx8J;{@D:EpRSYymR');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
