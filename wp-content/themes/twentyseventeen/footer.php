<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>

		</div><!-- #content -->

		<footer id="colophon" class="site-footer <?php if (is_page('home')) { echo "home-footer"; } ?>" role="contentinfo" >
			<div class="wrap-1342">
				<div class="footer-inr clearfix">
					<div class="ftr-newsletter hidden-md-up">
						<h3 class="ftr-hding">Nieuwsbrief</h3>
						<div class="search-div">
							<input type="text" class="form-control" placeholder="Inschrijven nieuwsbrief">
							<input class="search-icon" type="button" name="" value="" />
						</div>
					</div>
					<div class="info-div">
						<h3 class="ftr-hding">Contact Kennisnetwerk CVA</h3>
						<div class="address-div">
							<img src="<?= get_template_directory_uri(); ?>/assets/images/placemarker.svg" alt="" />
							<p>Postadres
							<span>Oudlaan 4, 3515 GA Utrecht, </span>
							<span>Netherlands</span>
							<span>p/a: MUMC+, RVE Patient en Zorg</span>
							<span>Postbus 5800</span>
							3515 GA Utrecht</p>

						</div>
						<div class="number-div">
							<a href="mailto:info@kennisnetwerkcva.nl">info@kennisnetwerkcva.nl</a>
							<p>Telefoon <strong>043 - 387 44 28</strong></p>
							<p>Bankrekeningnummer: IBAN</p>
							<p>NL19RABO0129519642</p>
						</div>
					</div>
					<div class="ftr-newsletter hidden-sm-down">
						<h3 class="ftr-hding">Nieuwsbrief</h3>
						<div class="search-div">
							<input type="text" class="form-control" placeholder="Inschrijven nieuwsbrief">
							<input class="search-icon" type="button" name="" value="" />
						</div>
					</div>
					<div class="ftr-social-links">
						<h3 class="ftr-hding">Volg ons</h3>
						<ul>
							<li>
								<a href="https://www.linkedin.com/company/kennisnetwerk-cva-nederland" target="_blank">
									<img src="<?= get_template_directory_uri(); ?>/assets/images/icon-linkedin.svg" alt="" />
								</a>
							</li>
							<li>
								<a href="https://twitter.com/KNCVANL" target="_blank">
									<img src="<?= get_template_directory_uri(); ?>/assets/images/icon-twitter.svg" alt="" />
								</a>
							</li>
							<li>
								<a href="https://vimeo.com/user71498979" target="_blank">
									<img src="<?= get_template_directory_uri(); ?>/assets/images/icon-vimeo.svg" alt="" />
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div><!-- .wrap -->
		</footer><!-- #colophon -->
	</div><!-- .site-content-contain -->
</div><!-- #page -->
<?php wp_footer(); ?>

	
    <script src="<?= get_template_directory_uri(); ?>/assets/js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="<?= get_template_directory_uri(); ?>/assets/js/ie10-viewport-bug-workaround.js"></script>
    <script src="<?= get_template_directory_uri(); ?>/assets/js/jquery.slimscroll.js"></script>
    <script src="<?= get_template_directory_uri(); ?>/assets/js/script.js"></script>

</body>
</html>
