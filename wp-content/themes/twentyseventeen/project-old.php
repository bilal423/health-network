<?php


/*Template Name: project_page*/

get_header();



$paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;

if(isset($_GET['category'])) {
	$c = get_term_by('slug', @$_GET['category'], 'project_categories');
	$args = array(
        'post_type' => 'project',
        'post_status' => 'publish',
        'orderby' => 'ID',
        'order'   => 'DESC',
        'meta_query'  => array(
	    	array(
	    		'key' => 'project_categories',
	    		'value' => $c->term_id,
	    		'compare' => 'LIKE'
	    	)
	    ),
        'posts_per_page' => '10',
        'paged' => $paged

    );
} else {
	$args = array(
	    'post_type' => 'project',
	    'post_status'  => 'publish',
	    'posts_per_page' => '10',
	    'orderby' => 'ID',
        'order'   => 'DESC',
        'paged' => $paged
    );
}

// print_r($args);

$project = new WP_Query( $args );
// $project = $project->posts;

?>


	<div class="news-page">
		<div class="wrap-1342">
			<div class="clearfix">
				<ul class="breadcrumb">
					<li class="breadcrumb-item"><a href="<?php echo site_url() . '/home' ; ?>">Home</a></li>
					<li class="breadcrumb-item">Projecten</li>
				</ul>
				<div class="news-left-section ">
					<h2 class="main-heading">Projecten</h2>
					<div class="news-left-inr project-div">
						<div class="project-top">
							<div class="select-style">
								
                                <select class="project-cat">
                                	<option class="project-cat02">Selecteer Een Thema</option>
                                	<?php $terms = get_terms([
										    'taxonomy' => 'project_categories',
										    'hide_empty' => false,
										]); 
									   foreach($terms as $term) {
									?>
                                    
                                    <option class="project-cat02" <?php if (@$term->slug == $_GET['category']) echo 'selected'; ?> value="<?= @$term->slug; ?>"><?php echo $term->name; ?></option>
                                    <?php } ?>
                                </select>
                                
                            </div>
                            <ul class="checkbox-project">
                            	<li>
                            		<input type="radio" id="c8" name="project_status" class="project-filter" value="all" />
									<label for="c8"><span></span>Toon alle projecten</label>
                            	</li>
                            	<li>
                            		<input type="radio" id="c6" name="project_status" class="project-filter" value="Afgeronde Projecten" />
									<label for="c6"><span></span>Afgeronde Projecten</label>
                            	</li>
                            	<li>
                            		<input type="radio" id="c7" name="project_status" class="project-filter"  value="Lopende Projecten" />
									<label for="c7"><span></span>Lopende Projecten</label>
                            	</li>
                            </ul>
						</div>
						<ul>
							<li class="clearfix">
								<ul class="project-con">
									<?php 

									if(sizeof($project->posts) > 0) { foreach($project->posts as $key => $p) {
									$meta = get_post_meta($p->ID); 
										
									?>
									<li>
										<div class="project-data-div">
											<h3><?= getProjectCategories( $p->ID );?> <?php if(getProjectStatus($p->ID)) { ?><img src="<?= get_template_directory_uri(); ?>/assets/images/tick-icon.png" alt="" /><?php } else { ?><img src="<?= get_template_directory_uri(); ?>/assets/images/dots-img.png" alt="" /><?php } ?></h3>
											<h4><?= @$p->post_title; ?></h4>
											<span><?= date('d-m-Y',strtotime(@$p->post_date)); ?></span>
											<?php
												$image = wp_get_attachment_image_src( get_post_thumbnail_id( $p->ID ), 'thumbnail' );

												if($image) { 
											?>
											<div class="project-img">
												<img src="<?php echo @$image[0]; ?>" alt="<?= @$p->post_title; ?>" />
											</div>
											<?php } ?>
											<div class="project-text">
												<h4><?= @$meta['stubs'][0];?></h4>
												<p><?= mb_strimwidth(@$p->post_excerpt, 0, 70, "...");?></p>
												<a href="<?php echo site_url() . '/project-details/?postId=' . $p->ID ; ?>">Meer Info</a>
											</div>
										</div>
									</li>
									<?php } } else { ?>
										<div class="news_post">
		                                    <h3>Geen projecten gevonden.</h3>
		                                </div>
									<?php } ?>
								</ul>
							</li>
						</ul>
					</div>
					<nav aria-label="Page navigation example">
						<ul class="pagination">
							<?php
								$big = 999999999; // need an unlikely integer

								echo paginate_links( array(
									'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
									'format' => '?paged=%#%',
									'current' => max( 1, get_query_var('paged') ),
									'total' => $project->max_num_pages
								) );
							?>
						</ul>
					</nav>
				</div>
				<div class="right-bar">
					<div class="category-list">
						<ul>
							<li>
								<a href="#">Ketens</a>
							</li>
							<li class="active">
								<a href="<?php echo site_url() . '/projects' ; ?>">Projecten</a>
							</li>
							<li>
								<a href="#">Brochures</a>
							</li>
							<li>
								<a href="#">Wetenschappelijk onderzoek</a>
							</li>
							<li>
								<a href="#">Publicaties</a>
							</li>
						</ul>
					</div>
					
				</div>
			</div>
		</div>
	</div>

<?php

get_footer();


?>