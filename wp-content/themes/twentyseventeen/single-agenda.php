
<?php


/*Template Name: single_agenda_page*/

get_header();

if(isset($_GET['postId']))
{
	$detail = get_post(@$_GET['postId']);
	$meta = get_post_meta(@$_GET['postId']);
} else {
	$detail = get_post(get_the_ID());
	$meta = get_post_meta(get_the_ID());
}

?>


	<div class="news-page">
		<div class="wrap-1342">
			<div class="clearfix">
				<ul class="breadcrumb">
					<li class="breadcrumb-item"><a href="<?php echo site_url() . '/home' ; ?>">Home</a></li>
					<li class="breadcrumb-item"><a href="<?php echo site_url() . '/agenda' ; ?>">Agenda</a></li>
					<li class="breadcrumb-item active"><?= mb_strimwidth(@$detail->post_title, 0, 30, "..."); ?></li>
				</ul>
				<div class="news-left-section single-agenda-div">
					<h2 class="main-heading"><?= @$detail->post_title;?></h2>
					<div class="single-agenda-inr">
						<div class="single-text">
							<p><strong><?= @$meta['time'][0];?></strong></p>
							<?= wpautop(@$detail->post_content);?>
						</div>
						<div class="spreker-div clearfix">
							<div class="spreker-left">
								<ul>
									<li>
										<h4>Thema</h4>
										<p><?= @$meta['theme'][0];?></p>
									</li>
									<li>
										<h4>Locatie</h4>
										<?php $location = 'https://www.google.com/maps/search/' . preg_replace('/[ ,]+/', '+', @$meta['location'][0]); ?> 
										<a href="<?= @$location; ?>"><?= @$meta['location'][0];?></a>
									</li>
									<?php if(wp_get_attachment_url( @$meta['bijlage'][0] )) : ?>
										<li>
											<h4>Bijlage</h4>
											<a href="<?php echo wp_get_attachment_url( @$meta['bijlage'][0] ); ?>" target="_blank"><?= get_the_title(@$meta['bijlage'][0]);?></a>
										</li>
									<?php endif?>
									<li>
										<h4>Informatie</h4>
										<a href="mailto:<?= @$meta['informatie'][0];?>"><?= @$meta['informatie'][0];?></a>
									</li>
									<li>
										<h4>Kosten</h4>
										<p><?= @$meta['kosten'][0];?></p>
									</li>
								</ul>
							</div>
						</div>
						<a class="btn anme-btn" href="#">Aanmelden</a>
						
						<div class="program-div">
							<?= wpautop(@$meta['agenda_details'][0]);?>
						</div>

						<?php
							if(isset($_GET['postId'])) {
								set_query_var( 'agenda_id',  @$_GET['postId']);
							} else {
								set_query_var( 'agenda_id',  get_the_ID());
							}
							 
							get_template_part('template-parts/speakers/content', 'speakers');
						?>
					</div>
				</div>
				<?php 
					get_template_part('template-parts/agenda/content', 'agenda');
				?>
			</div>
		</div>
	</div>

<?php

get_footer();


?>