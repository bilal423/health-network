<?php


/*Template Name: news_page*/

get_header();




$paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;

if(isset($_GET['term'])) {
	$c = get_term_by('slug', @$_GET['term'], 'news_categories');
	$args = array(
        'post_type' => 'news_cats',
        'post_status' => 'publish',
        'orderby' => 'ID',
        'order'   => 'DESC',
        'meta_query'  => array(
	    	array(
	    		'key' => 'nieuws_categories',
	    		'value' => $c->term_id,
	    		'compare' => 'LIKE'
	    	)
	    ),
        'posts_per_page' => '10',
        'paged' => $paged

    );
} else {
	$args = array(
	    'post_type' => 'news_cats',
	    'post_status'  => 'publish',
	    'posts_per_page' => '10',
	    'orderby' => 'ID',
        'order'   => 'DESC',
        'paged' => $paged
    );
}


$news = new WP_Query( $args );


?>


	<div class="news-page">
		<div class="wrap-1342">
			<div class="clearfix">
				<ul class="breadcrumb">
					<li class="breadcrumb-item"><a href="<?php echo site_url() . '/home' ; ?>">Home</a></li>
					<?php 
						if(isset($_GET['term'])) { ?>
						<li class="breadcrumb-item"><a href="<?php echo site_url() . '/news' ; ?>">Nieuws</a></li>
						<li class="breadcrumb-item"><?php echo @$_GET['term'] ; ?></li>
						
					<?php } else { ?>
						<li class="breadcrumb-item">Nieuws</li>
					<?php } ?>
				</ul>
				<div class="news-left-section ">
					<h2 class="main-heading">Nieuws</h2>
					<div class="news-left-inr news-left-inr03">
						<ul>
							<?php
                                if(sizeof($news->posts) > 0) { foreach($news->posts as $k => $new) { 
                                ?>
							<li>
								<div class="news-img">
									<?php if (has_post_thumbnail( $new->ID ) ): ?>
                                        <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $new->ID ), 'thumbnail' ); ?>
									<img src="<?php echo @$image[0]; ?>" alt="" />
									<?php endif; ?> 
								</div>
								<div class="news-text">
									<h3><a href="<?php echo site_url() . '/news-details/?postId=' . $new->ID ; ?>"><?= @$new->post_title;?></a></h3>
									<p class="post-date"><a href='<?php echo site_url() . "/news/?term=".getNewsCategoriesSlug($new->ID);?>'><?= getNewsCategories($new->ID); ?></a> <?= date('d-m-Y',strtotime(@$new->post_date)); ?></p>
									<p><?= @$new->post_excerpt;?></p>

									<a href="<?php echo site_url() . '/news-details/?postId=' . $new->ID ; ?>">lees verder</a>
								</div>
							</li>
							<?php } } else {  ?>
								<div class="news_post">
                                    <h3>Geen nieuws gevonden.</h3>
                                </div>
                            <?php } ?>
						</ul>
					</div>
					<nav aria-label="Page navigation example">
						<ul class="pagination">
							<?php
								$big = 999999999; // need an unlikely integer

								echo paginate_links( array(
									'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
									'format' => '?paged=%#%',
									'current' => max( 1, get_query_var('paged') ),
									'total' => $news->max_num_pages
								) );
							?>
						</ul>
					</nav>
				</div>
				<?php  get_template_part('template-parts/news/content', 'sidebar');?>
			</div>
		</div>
	</div>




<?php

get_footer();


?>
