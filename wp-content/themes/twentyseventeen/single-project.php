<?php


/*Template Name: project_detail_page*/

get_header();

$pdetail = get_post(@$_GET['postId']);
$meta = get_post_meta(@$_GET['postId']);
// print_r($meta);

?>


	<div class="news-page">
		<div class="wrap-1342">
			<div class="clearfix">
				<ul class="breadcrumb">
					<li class="breadcrumb-item"><a href="<?php echo site_url() . '/home' ; ?>">Home</a></li>
					<li class="breadcrumb-item"><a href="<?php echo site_url() . '/projects' ; ?>">Projecten</a></li>
					<li class="breadcrumb-item active"><?= mb_strimwidth(@$pdetail->post_title, 0, 30, "..."); ?></li>

				</ul>
				<div class="news-left-section">
					<h2 class="main-heading main-heading02"><?= @$pdetail->post_title;?></h2>
					<span class="hding-date"><?= date('d-m-Y',strtotime(@$pdetail->post_date)); ?></span>
					<div class="news-left-inr project-detail-div">
						<div class="news-detail-pg">

							<div class="detail-inr">
								<?php if (has_post_thumbnail( $pdetail->ID ) ): ?>
		                        <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $pdetail->ID ), 'full' ); ?>
								<img src="<?php echo $image[0]; ?>" alt="" />
								<?php endif; ?>
								<p><?= @$pdetail->post_content;?></p>
								<?php if(@$meta['attachments'][0]) { ?>
									<h3>Downloads</h3>
									<a href="<?php echo wp_get_attachment_url( @$meta['attachments'][0] ); ?>" download class="attachment"><img src="<?= get_template_directory_uri(); ?>/assets/images/pdf-icon.png" alt="" /><?= get_the_title(@$meta['attachments'][0]);?></a>
								<?php } ?>
								<div class="next-btns next-btns02">
									<ul>

										<li>
											<a class="blue-btn" href="#"><img src="<?= get_template_directory_uri(); ?>/assets/images/left-arrow.png" alt="" /> Vorige</a>
										</li>
										<li>
											<a class="blue-btn" href="#">Volgende <img src="<?= get_template_directory_uri(); ?>/assets/images/right-arrow.png" alt="" /></a>
										</li>
									</ul>
									<a class="citizn" href="<?php echo site_url() . '/projects' ; ?>">Terug naar overzicht</a>
								</div>
							</div>
						</div>

						<?php  get_template_part('template-parts/projects/content', 'related');?>
						
					</div>
				</div>
				<div class="right-bar">
					<div class="category-list project-links">
						<ul>
							<li>
								<a href="#">Ketens</a>
							</li>
							<li class="active">
								<a href="<?php echo site_url() . '/projects' ; ?>">Projecten</a>
							</li>
							<li>
								<a href="#">Brochures</a>
							</li>
							<li>
								<a href="#">Wetenschappelijk onderzoek</a>
							</li>
							<li>
								<a href="#">Publicaties</a>
							</li>
						</ul>
					</div>
					<?php $gallery = get_gallery( @$_GET['postId'] ); if (sizeof($gallery) > 0) { ?>
					<div class="sidebar-logo-div">
						<ul>
							<?php 
								 foreach ($gallery as $key => $ga) {
							?>
							<li>
								<a href="#">
									<img src="<?php echo $ga->large_url ?>" alt="<?php echo $ga->alt ?>" />
								</a>
							</li>
							<?php }  ?>
						</ul>
					
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>

<?php

get_footer();


?>