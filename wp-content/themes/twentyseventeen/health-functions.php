<?php 



add_action('wp_ajax_nopriv_agenda_filters','agenda_filters');
add_action('wp_ajax_agenda_filters','agenda_filters');

function agenda_filters() {

	$resp = "";
	$data = [];
	$d = [];
	$filter_val = [];
	$filter = explode(',', @$_POST['filter_value']);
	foreach ($filter as $key => $value) {
		array_push($filter_val, $value);
	}
	if(strtolower(@$_POST['filter_level']) == "archive") {
		$compare = "<";
	} else {
		$compare = ">=";
	}

	if(@$_POST['filter_value']) {
		$args = array(
		    'post_type' => 'agenda',
		    'post_status'  => 'publish',
		    'orderby' => 'agenda_date',
    		'order'   => 'ASC',
	        'meta_query'  => array(
		    	'relation' => 'AND',
		    	array(
		    		'key' => 'agenda_date',
		    		'value' => date('Ymd'),
		    		'compare' => $compare,
		    		'type' => 'DATE'
		    	),
		    	array(
	                'key' => 'organized_by',
	                'value'    => $filter_val,
	                'compare'  => 'IN',
	                // 'type'    => 'STRING'
	                
	            )
		    )
		);
		
	} else {
		$args = array(
		    'post_type' => 'agenda',
		    'post_status'  => 'publish',
		    'orderby' => 'agenda_date',
    		'order'   => 'ASC',
	        'meta_query'  => array(
		    	array(
		    		'key' => 'agenda_date',
		    		'value' => date('Ymd'),
		    		'compare' => $compare,
		    		'type' => 'DATE'
		    	),
		    )
		);
	}
	// print_r($args);
	$agenda = new WP_Query( $args );
	if(sizeof($agenda->posts) > 0) {
		foreach ($agenda->posts as $key => $ag) {
			$c = getAgendaSupportedClass(@$ag->ID);
			$resp .= '<li><div class="date-div '.$c.'">';
			$date = getAgendaDate($ag->ID);
			$month = getAgendaMonth($ag->ID);
			$resp .= '<p>'.$date.'<span>'.$month.'</span></p></div>';
			$url = site_url() . '/agenda-detail-page?postId=' . $ag->ID ;
			$resp .= '<div class="calender-text"><h3><a href='.$url.'>'.ucfirst(@$ag->post_title).'</a></h3><p>'.@$ag->post_excerpt.'</p></div></li>';

		}
	} else {
		$resp = "<p class='no-posts'>Geen agenda gevonden.</p>";
	}
	$data['data'] = $resp;
	echo json_encode($data);
	die();
}

add_action('wp_ajax_nopriv_project_filters','project_filters');
add_action('wp_ajax_project_filters','project_filters');

function project_filters() {
	$resp = "";
	$data = [];
	$d = [];

	if(isset($_POST['category'])) {
		$c = get_term_by('slug', @$_POST['category'], 'project_categories');
		if(@$_POST['filter_value'] == "all") {
			 $args = array(
			    'post_type' => 'project',
			    'post_status'  => 'publish',
			    'orderby' => 'ID',
	    		'order'   => 'DESC',
		        'meta_query'  => array(
			    	array(
		                'key' => 'project_categories',
		                'value'    => $c->term_id,
		                'compare' => 'LIKE'
		            )
			    )
			);
		} else {
			$args = array(
			    'post_type' => 'project',
			    'post_status'  => 'publish',
			    'orderby' => 'ID',
	    		'order'   => 'DESC',
		        'meta_query'  => array(
			    	array(
			    		'key' => 'status',
			    		'value' => @$_POST['filter_value'],
			    	),
			    	array(
		                'key' => 'project_categories',
		                'value'    => $c->term_id,
		                'compare' => 'LIKE'
		            )
			    )
			);
		}
	}

	else if(@$_POST['filter_value'] != "all") {
		$args = array(
		    'post_type' => 'project',
		    'post_status'  => 'publish',
		    'orderby' => 'ID',
    		'order'   => 'DESC',
	        'meta_query'  => array(
		    	array(
		    		'key' => 'status',
		    		'value' => @$_POST['filter_value'],
		    	),
		    )
		);
	} else {
		$args = array(
		    'post_type' => 'project',
		    'post_status'  => 'publish',
		    'orderby' => 'ID',
    		'order'   => 'DESC'
		);
	}		
	$projects = new WP_Query( $args );
	if(sizeof($projects->posts) > 0) {
		foreach ($projects->posts as $key => $project) {
			$resp .= '<li><div class="project-data-div">';
			$c = getProjectCategories( $project->ID );
			$resp .= '<h3>'.$c;
			if(getProjectStatus($project->ID)) {
				$resp .= '<img src="'.get_template_directory_uri().'/assets/images/tick-icon.png" alt="" />';
			} else {
				$resp .= '<img src="'.get_template_directory_uri().'/assets/images/dots-img.png" alt="" />';
			}
			$resp .= '<h3>';
			$resp .= '<h4>'.@$project->post_title.'</h4>';
			$resp .= '<span>'.date("d-m-Y",strtotime(@$project->post_date)).'</span>';
			$image = wp_get_attachment_image_src( get_post_thumbnail_id( @$project->ID ), 'thumbnail' );
			if($image) {
				$resp .= '<div class="project-img"><img src="'.@$image[0].'" alt="'.@$project->post_title.'" /></div>';
			}
			$resp .= '<div class="project-text"><h4>'.@$meta['stubs'][0].'</h4>';
			$resp .= '<p>'.mb_strimwidth(@$project->post_excerpt, 0, 70, "...").'</p>';
			$url = site_url() . '/project-details/?postId=' . $project->ID ;
			$resp .= '<a href="'.$url.'">Meer Info</a>';
			$resp .= '</div></div></li>';
		}
	} else {
		$resp .= '<div class="news_post"><h3>Geen projecten gevonden.</h3></div>';
	}
	$data['data'] = $resp;
	echo json_encode($data);
	die();
}


