<?php
/**
 * Widget API: Widget_Block class
 *
 * @package WordPress
 * @subpackage Widgets
 * @since 4.4.0
 */

/**
 * Core class used to implement a Block widget.
 *
 * @since 2.8.0
 *
 * @see WP_Widget
 */
class Widget_Block extends WP_Widget {

	public function __construct() {
		$widget_ops = array(
			'classname' => 'widget_block',
			'description' => __( 'To add images into generic pages.' ),
			'customize_selective_refresh' => true,
		);
		parent::__construct( 'block', _x( 'Block', 'Generic Page Utility' ), $widget_ops );
	}

	public function widget( $args, $instance ) {
		$title = ! empty( $instance['title'] ) ? $instance['title'] : '';

		/** This filter is documented in wp-includes/widgets/class-wp-widget-pages.php */
		$title = apply_filters( 'widget_title', $title, $instance, $this->id_base );

		echo $args['before_widget'];
		if ( $title ) {
			echo $args['before_title'] . $title . $args['after_title'];
		}

		// Use current theme search form if it exists
		get_search_form();

		echo $args['after_widget'];
	}

	public function form( $instance ) {
		$instance = wp_parse_args( (array) $instance, array( 'title' => '', 'description' => '') );
		

		print_r($instance);

		// die();


		$title = $instance['title'];
		$description = $instance['description'];
		?>
		<p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?> <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" /></label></p>
		<p><label for="<?php echo $this->get_field_id('description'); ?>"><?php _e('Description:'); ?><input class="widefat" id="<?php echo $this->get_field_id('description'); ?>" name="<?php echo $this->get_field_name('description'); ?>" type="text" value="<?php echo esc_attr($description); ?>" /></label></p>
		<?php
	}
	
}


function myplugin_register_widgets() {
	register_widget( 'Widget_Block' );
}
add_action( 'widgets_init', 'myplugin_register_widgets' );