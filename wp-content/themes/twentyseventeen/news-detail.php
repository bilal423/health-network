<?php


/*Template Name: news_detail_page*/


get_header();

global $post;

$details = get_post(@$_GET['postId']);
;
// echo "<pre>";
// print_r(get_previous_post());
// echo "</pre>";
// print_r(get_next_post());
// print_r(get_permalink(get_adjacent_post(false,'',true)));
// print_r(get_permalink(get_adjacent_post(false,'',false)));

// $link = get_permalink( get_adjacent_post( false, "", false ) );

// $args = array(

// 'post_type' => 'news_cats',

// 'posts_per_page' => 1,

// 'order' => 'ASC'

// );

// $last = new WP_Query($args);
// echo "<pre>";
// print_r($last);
// echo "</pre>";
// if ( $last->have_posts() ) {

// while ( $last->have_posts() ){

// $last->the_post();

// $link = get_permalink();

// }

// }

// wp_reset_query();


// print_r($link);
?>


	<div class="news-page">
		<div class="wrap-1342">
			<div class="clearfix">
				<ul class="breadcrumb">
					<li class="breadcrumb-item"><a href="<?php echo site_url() . '/home' ; ?>">Home</a></li>
					<li class="breadcrumb-item"><a href="<?php echo site_url() . '/news' ; ?>">Nieuws</a></li>
					<li class="breadcrumb-item active"><?= mb_strimwidth(@$details->post_title, 0, 30, "...");?></li>
				</ul>
				<?php  
					set_query_var( 'post_id',  @$_GET['postId']);
					get_template_part('template-parts/news/content', 'news');
				?>
				<?php  get_template_part('template-parts/news/content', 'sidebar');?>
			</div>
		</div>
	</div>

	<div class="navigation"><p><?php posts_nav_link(); ?></p></div>



<?php

print_r(previous_post_link('%link','Next Post'));
print_r(next_post_link());

?>
<?php

get_footer();


?>