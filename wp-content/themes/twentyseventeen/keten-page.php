<?php


/*Template Name: keten_detail_page*/

get_header();


?>


	<div class="news-page prod_det">
		<div class="wrap-1342">
			<div class="clearfix">
				<ul class="breadcrumb">
					<li class="breadcrumb-item"><a href="<?php echo site_url() . '/home' ; ?>">Home</a></li>
					<li class="breadcrumb-item"><a href="#">Zorg in regio</a></li></li>
					<li class="breadcrumb-item active">Zorgketen Gelderse Vallei</li>
				</ul>
				<div class="news-left-section">
					<h2 class="main-heading">Zorgketen Gelderse Vallei</h2>
					<div class="news-left-inr project-detail-div">
						<div class="news-left-section news-detail-pg">
							<div class="detail-inr">
								<p>Dolor ons adipi scing elite mauris urna adipi scing elite mauris urna tincidunt quia. Dolor ons adipi scing elite mauris urna adipi mauris urna tincidunt quia. dolor ons adipi scing elite mauris urna tincidunt quia. Dolor ons adipi scing elite mauris urna adipi mauris urna tinscing elite mauris urna adipi scing elite mauris urna tincidunt quia. Ipsum dolor ons adipi scing elite mauris urna adipi mauris urna tincidunt quia.</p>   
								<p>Ipsum dolor ons adipi scing elite mauris urna adipi scing elite mauris urna tincidunt quia. Dolor ons adipi scing elite mauris urna adipi mauris urna tincidunt quia. dolor ons adipi scing elite maer uuris urna adipi scing elite mauris urna tincidunt quia. Ipsum dolor ons adipi scing elite mghi ioauris urna adipi mauris urna tincidunt quia.</p>
							</div>
						</div>
						<div class="news-left-inr news-left-inr02 contact-inr keten_inr">
							<h2 class="main-heading">Aangesloten bij Zorgketen Gelderse Vallei</h2>
							<ul>
								<li>
									<div class="project-data-div keten_div clearfix">
										<div class="project-text">
											<h3>Instelling 1 Gelderse Vallei</h3>
											<p><span><img src="<?= get_template_directory_uri(); ?>/assets/images/location-icon.png" alt="" /></span> Laan van Vredenoord 1, 2289 DA Rijswijk</p>
											<p><span><img src="<?= get_template_directory_uri(); ?>/assets/images/phone-icon.png" alt="" /></span> 070 3456789</p>
											<p><span><img src="<?= get_template_directory_uri(); ?>/assets/images/website-logo.png" alt="" /></span> <a href="#">Website</a></p>
										</div>
										<div class="project-img">
											<p>Logo Instelling</p>
											<!-- <img src="<?= get_template_directory_uri(); ?>/assets/images/website-logo.png" alt="" /> -->
										</div>
									</div>
								</li>
								<li>
									<div class="project-data-div keten_div clearfix">
										<div class="project-text">
											<h3>Instelling 1 Gelderse Vallei</h3>
											<p><span><img src="<?= get_template_directory_uri(); ?>/assets/images/location-icon.png" alt="" /></span> Laan van Vredenoord 1, 2289 DA Rijswijk</p>
											<p><span><img src="<?= get_template_directory_uri(); ?>/assets/images/phone-icon.png" alt="" /></span> 070 3456789</p>
											<p><span><img src="<?= get_template_directory_uri(); ?>/assets/images/website-logo.png" alt="" /></span> <a href="#">Website</a></p>
										</div>
										<div class="project-img">
											<p>Logo Instelling</p>
											<!-- <img src="<?= get_template_directory_uri(); ?>/assets/images/website-logo.png" alt="" /> -->
										</div>
									</div>
								</li>
								<li>
									<div class="project-data-div keten_div clearfix">
										<div class="project-text">
											<h3>Instelling 1 Gelderse Vallei</h3>
											<p><span><img src="<?= get_template_directory_uri(); ?>/assets/images/location-icon.png" alt="" /></span> Laan van Vredenoord 1, 2289 DA Rijswijk</p>
											<p><span><img src="<?= get_template_directory_uri(); ?>/assets/images/phone-icon.png" alt="" /></span> 070 3456789</p>
											<p><span><img src="<?= get_template_directory_uri(); ?>/assets/images/website-logo.png" alt="" /></span> <a href="#">Website</a></p>
										</div>
										<div class="project-img">
											<p>Logo Instelling</p>
											<!-- <img src="<?= get_template_directory_uri(); ?>/assets/images/website-logo.png" alt="" /> -->
										</div>
									</div>
								</li>
								<li>
									<div class="project-data-div keten_div clearfix">
										<div class="project-text">
											<h3>Instelling 1 Gelderse Vallei</h3>
											<p><span><img src="<?= get_template_directory_uri(); ?>/assets/images/location-icon.png" alt="" /></span> Laan van Vredenoord 1, 2289 DA Rijswijk</p>
											<p><span><img src="<?= get_template_directory_uri(); ?>/assets/images/phone-icon.png" alt="" /></span> 070 3456789</p>
											<p><span><img src="<?= get_template_directory_uri(); ?>/assets/images/website-logo.png" alt="" /></span> <a href="#">Website</a></p>
										</div>
										<div class="project-img">
											<p>Logo Instelling</p>
											<!-- <img src="<?= get_template_directory_uri(); ?>/assets/images/website-logo.png" alt="" /> -->
										</div>
									</div>
								</li>
								<li>
									<div class="project-data-div keten_div clearfix">
										<div class="project-text">
											<h3>Instelling 1 Gelderse Vallei</h3>
											<p><span><img src="<?= get_template_directory_uri(); ?>/assets/images/location-icon.png" alt="" /></span> Laan van Vredenoord 1, 2289 DA Rijswijk</p>
											<p><span><img src="<?= get_template_directory_uri(); ?>/assets/images/phone-icon.png" alt="" /></span> 070 3456789</p>
											<p><span><img src="<?= get_template_directory_uri(); ?>/assets/images/website-logo.png" alt="" /></span> <a href="#">Website</a></p>
										</div>
										<div class="project-img">
											<p>Logo Instelling</p>
											<!-- <img src="<?= get_template_directory_uri(); ?>/assets/images/website-logo.png" alt="" /> -->
										</div>
									</div>
								</li>
								<li>
									<div class="project-data-div keten_div clearfix">
										<div class="project-text">
											<h3>Instelling 1 Gelderse Vallei</h3>
											<p><span><img src="<?= get_template_directory_uri(); ?>/assets/images/location-icon.png" alt="" /></span> Laan van Vredenoord 1, 2289 DA Rijswijk</p>
											<p><span><img src="<?= get_template_directory_uri(); ?>/assets/images/phone-icon.png" alt="" /></span> 070 3456789</p>
											<p><span><img src="<?= get_template_directory_uri(); ?>/assets/images/website-logo.png" alt="" /></span> <a href="#">Website</a></p>
										</div>
										<div class="project-img">
											<p>Logo Instelling</p>
											<!-- <img src="<?= get_template_directory_uri(); ?>/assets/images/website-logo.png" alt="" /> -->
										</div>
									</div>
								</li>
								<li>
									<div class="project-data-div keten_div clearfix">
										<div class="project-text">
											<h3>Instelling 1 Gelderse Vallei</h3>
											<p><span><img src="<?= get_template_directory_uri(); ?>/assets/images/location-icon.png" alt="" /></span> Laan van Vredenoord 1, 2289 DA Rijswijk</p>
											<p><span><img src="<?= get_template_directory_uri(); ?>/assets/images/phone-icon.png" alt="" /></span> 070 3456789</p>
											<p><span><img src="<?= get_template_directory_uri(); ?>/assets/images/website-logo.png" alt="" /></span> <a href="#">Website</a></p>
										</div>
										<div class="project-img">
											<p>Logo Instelling</p>
											<!-- <img src="<?= get_template_directory_uri(); ?>/assets/images/website-logo.png" alt="" /> -->
										</div>
									</div>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="right-bar quality-rightbar">
					<div class="category-list">
						<ul>
							<li>
						    	<a href="#">Wat is een CVA</a>
						    </li>
							<li>
								<a href="#">Veel gestelde vragen</a>
							</li>
							<li class="active">
								<a href="#">Zorg in uw regio</a>
							</li>
							<li>
								<a href="#">Brochures</a>
							</li>
						</ul>
					</div>
					<div class="news-brief-div keten-side">
						<h3>Ketencoordinator</h3>
						<p>Voornaam Achternaam</p>
						<p>Instellin</p>
						<p>Functie</p>
						<p><span><img src="<?= get_template_directory_uri(); ?>/assets/images/phone-icon.png" alt="" /></span> 06 12345678</p>
						<p><span><img src="<?= get_template_directory_uri(); ?>/assets/images/mail-icon.png" alt="" /></span> <a href="#">achternaam@instelling.nl</a></p>
					</div>
					<div class="news-brief-div keten-side keten-down">
						<h3>Document Voor Patienten</h3>
						<p><img src="<?= get_template_directory_uri(); ?>/assets/images/pdf-icon02.png" alt="" /> <a href="#">Sociale Kaart</a></p>
						<p><img src="<?= get_template_directory_uri(); ?>/assets/images/pdf-icon02.png" alt="" /> <a href="#">Folder 1 voor Patienten</a></p>
						<p><img src="<?= get_template_directory_uri(); ?>/assets/images/pdf-icon02.png" alt="" /> <a href="#">Folder 2 voor Patienten</a></p>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php

get_footer();


?>