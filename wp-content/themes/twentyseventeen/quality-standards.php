<?php


/*Template Name: quality_standards_page*/

get_header();

$args = array(
    'post_type' => 'quality_standards',
    'post_status'  => 'publish'
);

$quality_standards = new WP_Query( $args );

?>


	<div class="news-page">
		<div class="wrap-1342">
			<div class="clearfix">
				<ul class="breadcrumb">
					<li class="breadcrumb-item"><a href="<?php echo site_url() . '/home' ; ?>">Home</a></li>
					<li class="breadcrumb-item"><a href="#">Ketens</a></li>
					<li class="breadcrumb-item active">Kwaliteitsstandaarden</li>
				</ul>
				<div class="news-left-section ">
					<h2 class="main-heading">Kwaliteitsstandaarden</h2>
					<div class="news-left-inr quality-main">
						<div class="project-top">
							<div class="select-style">
                                <select>
                                    <option value="please-select">Selecteer Een Thema</option>
                                    <?php $terms = get_terms([
										    'taxonomy' => 'qs_categories',
										    'hide_empty' => false,
										]); 
									   foreach($terms as $term) {
									?>
                                    <option value="<?= @$term->slug; ?>"><?php echo $term->name; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
						</div>
						<ul>
							<li class="clearfix">
								<ul>
									<?php 

									if(sizeof($quality_standards->posts) > 0) { foreach($quality_standards->posts as $key => $qs) {
									$meta = get_post_meta($qs->ID);
										
									?>
									<li>
										<div class="project-data-div quality-div">
											<h3 <?= $qs->ID; ?>><?= getQualityCategories( $qs->ID );?></h3>
											<h4><a href="<?php echo site_url() . '/quality-standards-details/?postId=' . $qs->ID ; ?>"><?= @$qs->post_title; ?></a></h4>
											<span><?= date('Y',strtotime(@$meta['publication_date'][0]));?> <?= @$meta['association'][0];?></span>
											<div class="project-text">
												<h4><?= @$meta['stub'][0] ;?></h4>
												<p><?= wpautop(mb_strimwidth(@$qs->post_excerpt, 0, 150, "..."));?></p>
												
												<a href="<?php echo wp_get_attachment_url( @$meta['attach_file'][0] ); ?>" download><img src="<?= get_template_directory_uri(); ?>/assets/images/pdf-icon.png" alt="" /><?= get_the_title(@$meta['attach_file'][0]);?></a>
											</div>
										</div>
									</li>
									<?php } } else { ?>
										<div class="news_post">
		                                    <h3>Geen kwaliteitsstandaarden gevonden.</h3>
		                                </div>
									<?php } ?>
								</ul>
							</li>
						</ul>
					</div>
				</div>
				<div class="right-bar quality-rightbar">
					<div class="category-list">
						<ul>
							<li>
						    	<a class="nav-link collapsed" href="#submenu1" data-toggle="collapse" data-target="#submenu1">Ketens</a>
			                    <div class="collapse" id="submenu1" aria-expanded="false">
			                        <ul class="flex-column pl-2 nav">
			                            <li class="nav-item"><a class="nav-link py-0" href="#">Jaarplannen</a></li>
			                            <li class="nav-item">
			                                <a class="nav-link py-1" href="#">Borging en sturing</a>
			                            </li>
			                            <li class="nav-item <?php if (is_page('quality-standards')) echo 'active'; ?>">
			                                <a class="nav-link py-1" href="<?php echo site_url() . '/quality-standards' ; ?>">Kwaliteitsstandaarden</a>
			                            </li>
			                            <li class="nav-item">
			                                <a class="nav-link py-1" href="#">Scholing & Training</a>
			                            </li>
			                            <li class="nav-item">
			                                <a class="nav-link py-1" href="#">Projectmatig Werken</a>
			                            </li>
			                        </ul>
			                    </div>
						    </li>
							<li class="<?php if (is_page('projects')) echo 'active'; ?>">
								<a href="<?php echo site_url() . '/projects' ; ?>">Projecten</a>
							</li>
							<li class="<?php if (is_page('brochures')) echo 'active'; ?>">
								<a href="<?php echo site_url() . '/brochures' ; ?>">Brochures</a>
							</li>
							<li>
								<a href="#">Wetenschappelijk onderzoek</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php

get_footer();


?>