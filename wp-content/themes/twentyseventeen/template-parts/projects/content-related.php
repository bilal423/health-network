<?php

/**
 * Template part for displaying speaker posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

$project_args = array(
    'post_type' => 'project',
    'post_status'  => 'publish',
    'posts_per_page' => '4',
    // 'meta_key'      => 'views', //the metakey previously defined
    'orderby'       => 'ID',
    'order'         => 'DESC'
);

$project = new WP_Query( $project_args );
// print_r($project->posts);
?>
<?php if(sizeof($project->posts) > 0) { ?>
<h2 class="scnd-hding">Anderen Bekeken ook</h2>
<ul>

	<li class="clearfix">
		<ul class="project-con">
			<?php foreach($project->posts as $key => $post) { $meta = get_post_meta($post->ID);  ?>
			<li>
				<div class="project-data-div">
					<h3><?= getProjectCategories( $post->ID );?></h3>
					<h4><?= $post->post_title;?></h4>
					<span><?= date('d-m-Y',strtotime(@$post->post_date)); ?></span>
					<?php
						$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'thumbnail' );

						if($image) { 
					?>
						<div class="project-img">
							<img src="<?php echo @$image[0]; ?>" alt="<?= @$post->post_title; ?>" />
						</div>
					<?php } ?>
					<div class="project-text">
						<h4><?= @$meta['stubs'][0];?></h4>
						<p><?= mb_strimwidth(@$post->post_excerpt, 0, 70, "...");?></p>
						<a href="<?php echo site_url() . '/project-details/?postId=' . $post->ID ; ?>">Meer Info</a>
					</div>
				</div>
			</li>
			<?php } ?>
		</ul>
	</li>
</ul>
<?php } ?>