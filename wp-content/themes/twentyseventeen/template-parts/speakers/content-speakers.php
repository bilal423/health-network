<?php

/**
 * Template part for displaying speaker posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */


$agenda_speakers = [];
if(@$agenda_id) { 
	$ag_speakers = [];
	$speakers = get_post_meta(@$agenda_id, 'speakers');
	if($speakers)  {
		foreach ($speakers[0] as $key => $speaker) {
			$post = get_post($speaker);
			$ag_speakers['title'] = $post->post_title;
			$ag_speakers['content'] = $post->post_excerpt;
			$ag_speakers['post_id'] = $post->ID;
			array_push($agenda_speakers, $ag_speakers);
		}
	}
}

?>

<?php
	

?>


<?php if(sizeof($agenda_speakers) > 0) { ?>

	<h2 class="main-heading main-heading01">Sprekers</h2>
	<div class="news-left-inr">
		<ul>
			<?php foreach ($agenda_speakers as $key => $speaker) { 
				$meta = get_post_meta($speaker['post_id']);
				?>
			<li>
				<?php if (has_post_thumbnail( $speaker['post_id'] ) ): ?>
                        
				<div class="news-img">
					<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $speaker['post_id'] ), 'full' ); ?>	
					<img src="<?php echo $image[0]; ?>" alt="" />
				</div>
				<?php endif; ?>
				<div class="news-text">
					<h3><?= @$speaker['title']; ?></h3>
					<p class="post-date"><?= @$meta['designation'][0];?></p>
					<p><?= @$speaker['content'];?></p>
					<a href="<?php echo wp_get_attachment_url( @$meta['attachment'][0] ); ?>">Presentatie</a>

				</div>
			</li>
			<?php } ?>
		</ul>
	</div>			
<?php } ?> 