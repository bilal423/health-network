<?php

/**
 * Template part for displaying speaker posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>


<div class="right-bar">
	<div class="category-list">
		<ul>
			<?php $terms = get_terms([
				    'taxonomy' => 'news_categories',
				    'hide_empty' => false,
				]); 
			   foreach($terms as $term) {
			?>
			<li>
				<a href="<?php echo site_url() . '/news/?term=' . $term->slug; ?>"><?php echo $term->name; ?></a>
			</li>
			<?php } ?>
		</ul>
		
	</div>
	<div class="news-brief-div">
		<h3>Inschrijven nieuwsbrief</h3>
		<?php
             echo do_shortcode(
               '[gravityform id="1" title="false" description="false"]'
             );
        ?>
	</div>
</div>