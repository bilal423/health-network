<?php

/**
 * Template part for displaying speaker posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */



	$details = get_post(@$post_id);

?>


<div class="news-left-section news-detail-pg">
	<h2 class="main-heading"><?= @$details->post_title;?></h2>
	<div class="detail-inr">
		<?php if (has_post_thumbnail( $details->ID ) ): ?>
        <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $details->ID ), 'full' ); ?>
		<img src="<?php echo $image[0]; ?>" alt="" />
		<?php endif; ?>
		<?= wpautop(@$details->post_content);?>
		<div class="next-btns">
			<ul>
				<li>
					<a class="blue-btn" href="#"><img src="<?= get_template_directory_uri(); ?>/assets/images/left-arrow.png" alt="" /> Vorige</a>
				</li>
				<li>
					<a class="blue-btn" href="#">Volgende <img src="<?= get_template_directory_uri(); ?>/assets/images/right-arrow.png" alt="" /></a>
				</li>
			</ul>
			<a class="citizn" href="<?php echo site_url() . '/news' ; ?>">Terug naar overzicht</a>
		</div>
		<!-- <input class="form-control" type="text" name="email" placeholder="Email" /> -->
	</div>
</div>



