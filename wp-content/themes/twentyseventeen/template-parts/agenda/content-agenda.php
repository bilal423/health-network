<?php


/**
 * Template part for displaying agenda posts for sidebar
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

$args = array(
    'post_type' => 'agenda',
    'post_status'  => 'publish',
    'posts_per_page' => '3',
    'orderby' => 'agenda_date',
    'order'   => 'ASC',
    'meta_query'  => array(
    	array(
    		'key' => 'agenda_date',
    		'value' => date('Ymd'),
    		'compare' => '>=',
    		'type' => 'DATE'
    	),
    )
);

$agenda = new WP_Query( $args );


?>


<div class="right-bar">
	<div class="news-brief-div single-agenda-bar">
		<h3>Agenda</h3>
		<div class="news-left-inr">
			<ul>
				<?php if(sizeof($agenda->posts) > 0 ) { foreach($agenda->posts as $k => $ad) {
				?>
				<li>
					<div class="date-div <?= getAgendaSupportedClass(@$ad->ID);?>">
						<p><?= getAgendaDate($ad->ID); ?> <span><?= getAgendaMonth($ad->ID); ?></span></p>
					</div>
					<div class="calender-text">
						<h3><a href="#"><?= @$ad->post_title;?></a></h3>
						<p><?= mb_strimwidth(@$ad->post_excerpt, 0, 70, "...");?></p>
					</div>
				</li>
				<?php } } else {  ?>
				<div class="news_post">
				    <h3>No agenda post found.</h3>
				</div>
				<?php } ?>
			</ul>
		</div>
	</div>
</div>
