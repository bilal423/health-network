<?php

/**
 * Template part for displaying speaker posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

// echo "board members";

$args = array(
    'post_type' => 'board_members',
    'post_status'  => 'publish',
    'posts_per_page' => '4'

);

$board = new WP_Query( $args );

// echo "<pre>";
// print_r($board);
// echo "</pre>";

?>

<?php if(sizeof($board->posts) > 0) { ?>

	<h2 class="main-heading main-heading01">Boardmembers</h2>
	<div class="news-left-inr">
		<ul>
			<?php foreach ($board->posts as $key => $b) {
				$meta = get_post_meta($b->ID);
				?>
			<li>
				<?php if (has_post_thumbnail( $b->ID ) ): ?>
                        
				<div class="news-img">
					<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $b->ID ), 'full' ); ?>	
					<img src="<?php echo $image[0]; ?>" alt="" />
				</div>
				<?php endif; ?>
				<div class="news-text">
					<h3><?= @$b->post_title; ?></h3>
					<p class="post-date"><?= @$meta['designation'][0];?></p>
					<p><?= @$b->post_excerpt;?></p>
					<a href="#">Prenestatie</a>
				</div>
			</li>
			<?php } ?>
		</ul>
	</div>			
<?php } ?>