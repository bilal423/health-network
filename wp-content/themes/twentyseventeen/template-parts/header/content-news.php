<?php/**
 * Template part for displaying speaker posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */




$args = array(
    'post_type' => 'news_cats',
    'post_status'  => 'publish',
    'posts_per_page' => '3',
    'orderby' => 'ID',
    'order'   => 'DESC'
);

$news = new WP_Query( $args );

?>

<div class="home-news-section">
	<h4 class="home-section-hding">Nieuws</h4>
	<ul>
		<?php if(sizeof($news->posts) > 0 ) { foreach ($news->posts as $key => $news) {
		?> 
		<li>
			<div class="home-news-inr">
				<h3><a href="<?php echo site_url() . '/news-details?postId=' . $news->ID ; ?>"><?= @$news->post_title; ?></a></h3>
				<div class="home-news-img">
					<?php
						$image = wp_get_attachment_image_src( get_post_thumbnail_id( $news->ID ), 'thumbnail' );
					?>
					<img src="<?php echo @$image[0]; ?>" alt="<?= @$news->post_title; ?>" />
				</div>
				<div class="home-news-text">
					<p><?= mb_strimwidth(@$news->post_excerpt, 0, 130, "..."); ?></p>
					<a href="<?php echo site_url() . '/news-details?postId='.$news->ID; ?>">Lees verder</a>
					<span><?= getNewsCategories($news->ID); ?> <?= date('d-m-Y',strtotime(@$news->post_date)); ?></span>
				</div>
			</div>
		</li>
		<?php } } ?>
	</ul>
</div>