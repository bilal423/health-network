<?php

/*Template Name: about-us-team*/

get_header();

// $detail = get_post(@$_GET['postId']);

// $meta = get_post_meta(@$_GET['postId']);


?>


	<div class="news-page">
		<div class="wrap-1342">
			<div class="clearfix">
				<ul class="breadcrumb">
					<li class="breadcrumb-item"><a href="<?php echo site_url() . '/home' ; ?>">Home</a></li>
					<li class="breadcrumb-item"><a href="<?php echo site_url() . '/about-us' ; ?>">About Us</a></li>
					<li class="breadcrumb-item active">Board Members</li>
				</ul>
				<div class="news-left-section single-agenda-div">
					<h2 class="main-heading">The Board</h2>
					<div class="single-agenda-inr">
						<div class="single-text about_text">
							<p>Introtekst CVA is de afkorting voor Cerebro Vasculair Accident. Een CVA kan een herseninfarct of hersenbloeding zijn en wordt ook wel een beroerte genoemd. Een herseninfarct komt het meeste voor. Bij 85% van de mensen met een CVA gaat het om een herseninfarct. Bij een herseninfarct wordt een bloedvat afgesloten door een bloedpropje. Hierdoor krijgt een deel van de hersenen te weinig of geen zuurstof. Een TIA is een licht, kortdurend herseninfarct, en kan een voorbode zijn voor een CVA. TIA staat voor Transient Ischemic Attack. Bij een hersenbloeding scheurt of knapt een bloedvat in de hersenen.</p>
						</div>

						<?php
							set_query_var( 'agenda_id',  @$_GET['postId']); 
							get_template_part('template-parts/board-members/content', 'board');
						?>
					</div>
				</div>
				<div class="right-bar">
					<div class="category-list">
						<ul>
							<li>
								<a href="<?php echo site_url() . '/about-us' ; ?>">Over ons</a>
							</li>
							<li>
								<a href="#">Missien</a>
							</li>
							<li>
								<a href="#">Activiteiten</a>
							</li>
							<li>
								<a href="#">Thema’s</a>
							</li>
							<li>
								<a href="<?php echo site_url() . '/about-us-team' ; ?>">Bestuursleden</a>
							</li>
						</ul>
					</div>
					<div class="about-side-div">
						<img src="<?= get_template_directory_uri(); ?>/assets/images/icon-quote.svg" alt="" />
						<p>Optional, room for a quote when there’s an interview with someone</p>
					</div>
					<div class="about-side-img">
						<img src="<?= get_template_directory_uri(); ?>/assets/images/about-sidebar-news-img.png" alt="" />
					</div>
				</div>
			</div>
		</div>
	</div>

<?php

get_footer();


?>