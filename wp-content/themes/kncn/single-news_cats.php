<?php



get_header();

global $post;

$details = get_post(get_the_ID());

?>


<div class="news-page">
	<div class="wrap-1342">
		<div class="clearfix">
			<ul class="breadcrumb">
				<li class="breadcrumb-item"><a href="<?php echo site_url() . '/home' ; ?>">Home</a></li>
				<li class="breadcrumb-item"><a href="<?php echo site_url() . '/news' ; ?>">Nieuws</a></li>
				<li class="breadcrumb-item active"><?= mb_strimwidth(@$details->post_title, 0, 30, "...");?></li>
			</ul>
			<?php  
				set_query_var( 'post_id',  get_the_ID());
				get_template_part('template-parts/news/content', 'news');
			?>
			<?php  get_template_part('template-parts/news/content', 'sidebar');?>
		</div>
	</div>
</div>


<?php get_footer(); ?>