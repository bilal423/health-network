<?php 


// Helper Functions Goes here


function getAgendaDate($postId) {
	if ($postId) {
		$date = get_post_meta($postId, 'agenda_date');
		$d = date('d',strtotime(@$date[0]));
		return $d;
	}
	die();
}

function getAgendaMonth($postId) {
	if ($postId) {
		$date = get_post_meta($postId, 'agenda_date');
		$m = date('M',strtotime(@$date[0]));
		return $m;
	}
	die();
}


function getAgendaSupportedClass($postId) {

	if($postId) {
		$terms = get_post_meta($postId,'organized_by')[0];
		if(strtolower($terms) == "kncn algemeen") {
			return "kncn";
		}  if(strtolower($terms) == "ketencoordinatoren") {
			return "ketens";
		}  if(strtolower($terms) == "knowledgebrokers") {
			return "knowledgebrokers";
		} if(strtolower($terms) == "extern") {
			return "extern";
		}

	}
}


function getNewsCategories($postId) {
	if($postId) {
		$terms = get_post_meta( $postId, 'nieuws_categories' );
		$terms = $terms[0];
		$term_key = "";
		if($terms) {
			foreach ($terms as $key => $term) {
				$t = get_term($term);
				$term_key .= $t->name.' ,';
			}
		}

		$term_key = rtrim($term_key,","); 
		return $term_key;
	}
}

function getNewsCategoriesSlug($postId) {
	if($postId) {
		$terms = wp_get_object_terms( $postId, 'news_categories', array( 'fields' => 'slugs' ) )[0];
		return $terms;
	}
}

function getProjectStatus($postId) {
	if ($postId) {
		$meta = get_post_meta($postId, 'status');
		if(strtolower($meta[0]) == 'afgeronde projecten') {
			return true;
		} else {
			return false;
		}
	}
}

function getProjectCategories($postId) {
	if($postId) {
		$terms = get_post_meta( $postId, 'project_categories' );
		$terms = $terms[0];
		$term_key = "";
		if($terms) {
			foreach ($terms as $key => $term) {
				$t = get_term($term);
				$term_key .= $t->name.' ,';
			}
		}

		$term_key = rtrim($term_key,","); 
		return $term_key;
	}
}

function getQualityCategories($postId) {
	if($postId) {
		$terms = get_post_meta( $postId, 'quality_categories' );
		$terms = $terms[0];
		$term_key = "";
		if($terms) {
			foreach ($terms as $key => $term) {
				$t = get_term($term);
				$term_key .= $t->name.' ,';
			}
		}

		$term_key = rtrim($term_key,","); 
		return $term_key;
	}
}

function wp_get_menu_array($current_menu) {

   $array_menu = wp_get_nav_menu_items($current_menu);
   $menu = array();
   foreach ($array_menu as $m) {
       if (empty($m->menu_item_parent)) {
           $menu[$m->ID] = array();
           $menu[$m->ID]['ID']      =   $m->ID;
           $menu[$m->ID]['title']       =   $m->title;
           $menu[$m->ID]['url']         =   $m->url;
           $menu[$m->ID]['children']    =   array();
       }
   }
   $submenu = array();
   foreach ($array_menu as $m) {
       if ($m->menu_item_parent) {
           $submenu[$m->ID] = array();
           $submenu[$m->ID]['ID']       =   $m->ID;
           $submenu[$m->ID]['title']    =   $m->title;
           $submenu[$m->ID]['url']  =   $m->url;
           $menu[$m->menu_item_parent]['children'][$m->ID] = $submenu[$m->ID];
       }
   }
   return $menu;
   
}



// function is_user_admin() {
// 	print_r(wp_get_current_user());
// }