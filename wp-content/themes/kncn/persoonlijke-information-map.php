<?php


/*Template Name: persoonlijkeinformationmap_page*/

get_header();


?>


	<div class="news-page prod_det">
		<div class="wrap-1342">
			<div class="clearfix">
				<ul class="breadcrumb">
					<li class="breadcrumb-item"><a href="<?php echo site_url() . '/home' ; ?>">Home</a></li>
					<li class="breadcrumb-item"><a href="#">Kennisbank</a></li></li>
					<li class="breadcrumb-item active">Persoonlijke Information Map</li>
				</ul>
				<div class="news-left-section">
					<h2 class="main-heading">Persoonlijke Information Map</h2>
					<div class="news-left-inr project-detail-div">
						<div class="news-detail-pg person_info">
							<div class="detail-inr">
								<p>Ipsum dolor ons adipi scing elite mauris urna adipi scing elite mauris urna tincidunt quia. Dolor ons adipi scing elite mauris urna adipi mauris urna tincidunt quia. dolor ons adipi scing elite mauris urna adipi scing elite mauris urna tincidunt quia. Ipsum dolor ons adipi scing elite mauris urna adipi mauris urna tincidunt quia.</p>   
								<p>Dolor ons adipi scing elite mauris urna adipi scing elite mauris urna tincidunt quia. Dolor ons adipi scing elite mauris urna adipi mauris urna tincidunt quia. dolor ons adipi scing elite mauris urna tincidunt quia. Dolor ons adipi scing elite mauris urna adipi mauris urna tinscing elite mauris urna adipi scing elite mauris urna tincidunt quia. Ipsum dolor ons adipi scing elite mauris urna adipi mauris urna tincidunt quia.</p>
							</div>
							<h3>Om dit project te verder te bekijken moet u lid zijnvan het kennisnetwerk CVA.</h3>
							<div class="broker-div member-div">
								<h3>Bent u lid, dan kunt u hier inloggen</h3>
								<input type="email" name="name" class="form-control" placeholder="Gebruikersnaam">
								<input type="password" name="name" class="form-control form-control02" placeholder="Wachtwoord">
								<a class="btn" href="#">Aanmelden</a>
								<a class="forget-paw" href="#">Wachtwoord vergeten?</a>
							</div>
							<div class="next-btns person-div">
								<ul>
									<li>
										<a class="blue-btn" href="#"><img src="<?= get_template_directory_uri(); ?>/assets/images/left-arrow.png" alt="" /> Vorige</a>
									</li>
									<li>
										<a class="blue-btn" href="#>">Volgende <img src="<?= get_template_directory_uri(); ?>/assets/images/right-arrow.png" alt="" /></a>
									</li>
								</ul>
								<a class="citizn" href="#">Terug naar overzicht</a>
							</div>
						</div>
					</div>
				</div>
				<div class="right-bar">
					<div class="category-list">
						<ul>
							<li>
								<a href="#">Ketens</a>
							</li>
							<li class="active">
								<a href="#">Projecten</a>
							</li>
							<li>
								<a href="#">Brochures</a>
							</li>
							<li>
								<a href="#">Wetenschappelijk onderzoek</a>
							</li>
							<li>
								<a href="#">Publicaties</a>
							</li>
						</ul>
					</div>
					<div class="sidebar-logo-div">
						<?php $gallery = get_gallery( @$_GET['postId'] ); ?>

						<ul>
							<li>
								<a href="#">
									<img src="<?= get_template_directory_uri(); ?>/assets/images/new-list-logo01.png" alt="" />
								</a>
							</li>
							<li>
								<a href="#">
									<img src="<?= get_template_directory_uri(); ?>/assets/images/new-list-logo02.png" alt="" />
								</a>
							</li>
							<li>
								<a href="#">
									<img src="<?= get_template_directory_uri(); ?>/assets/images/new-list-logo03.png" alt="" />
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php

get_footer();


?>