<?php


/*Template Name: about_page*/



get_header();

if(isset($_GET['term'])) {
	$args = array(
        'post_type' => 'news',
        'post_status' => 'publish',
        'tax_query' => array(
            array(
                'taxonomy' => 'news_categories',
                'field'    => 'slug',
                'terms'    => @$_GET['term']
            )
        )
    );
} else {
	$args = array(
	    'post_type' => 'news',
	    'post_status'  => 'publish'
    );
}

$news = new WP_Query( $args );
$news = $news->posts;

?>


	<div class="news-page">
		<div class="wrap-1342">
			<div class="clearfix">
				<ul class="breadcrumb">
					<li class="breadcrumb-item"><a href="<?php echo site_url() . '/home' ; ?>">Home</a></li>
					<?php 
						if(isset($_GET['term'])) { ?>
						<li class="breadcrumb-item"><a href="<?php echo site_url() . '/about-us' ; ?>">About Us</a></li>
						<li class="breadcrumb-item"><?php echo @$_GET['term'] ; ?></li>
						
					<?php } else { ?>
						<li class="breadcrumb-item">About Us</li>
					<?php } ?>
				</ul>
				<div class="news-left-section ">
					<h2 class="main-heading">Activiteiten KNCN (H1)</h2>
					<p class="news-peragraph">Introtekst CVA is de afkorting voor Cerebro Vasculair Accident. Een CVA kan een herseninfarct of hersenbloeding zijn en wordt ook wel een beroerte genoemd. Een herseninfarct komt het meeste voor. Bij 85% van de mensen met een CVA gaat het om een herseninfarct. Bij een herseninfarct wordt een bloedvat afgesloten door een bloedpropje. Hierdoor krijgt een deel van de hersenen te weinig of geen zuurstof. Een TIA is een licht, kortdurend herseninfarct, en kan een voorbode zijn voor een CVA. TIA staat voor Transient Ischemic Attack. Bij een hersenbloeding scheurt of knapt een bloedvat in de hersenen.</p>
					<div class="news-left-inr news-left-inr02 about-left-inr">
						<ul>
							<li>
								<h3 class="news-post-hding">Leernetwerk (H2)</h3>
								<div class="news-img">
									<img src="<?= get_template_directory_uri(); ?>/assets/images/news-img.png" alt="" />
								</div>
								<div class="news-text">
									<p>Bij een CVA wordt de stroom van het bloed naar een deel van de hersenen verstoord. Hierdoor krijgt het hersenweefsel te weinig zuurstof en raakt het beschadigd. Dat deel kan niet meer functioneren. Dit verlies kan tijdelijk of blijvend zijn. Direct na een...</p>
									<a href="#">Less Verder</a>
								</div>
							</li>
							<li>
								<h3 class="news-post-hding">Refelctie op kwaliteit</h3>
								<div class="news-img">
									<img src="<?= get_template_directory_uri(); ?>/assets/images/news-img.png" alt="" />
								</div>
								<div class="news-text">
									<p>Bij een CVA wordt de stroom van het bloed naar een deel van de hersenen verstoord. Hierdoor krijgt het hersenweefsel te weinig zuurstof en raakt het beschadigd. Dat deel kan niet meer functioneren. Dit verlies kan tijdelijk of blijvend zijn. Direct na een...</p>
									<a href="#">Less Verder</a>
								</div>
							</li>
							<li>
								<h3 class="news-post-hding">Scholing & onderzoek</h3>
								<div class="news-img">
									<img src="<?= get_template_directory_uri(); ?>/assets/images/news-img.png" alt="" />
								</div>
								<div class="news-text">
									<p>Bij een CVA wordt de stroom van het bloed naar een deel van de hersenen verstoord. Hierdoor krijgt het hersenweefsel te weinig zuurstof en raakt het beschadigd. Dat deel kan niet meer functioneren. Dit verlies kan tijdelijk of blijvend zijn. Direct na een...</p>
									<a href="#">Less Verder</a>
								</div>
							</li>
							<li>
								<h3 class="news-post-hding">Leernetwerk (H2)</h3>
								<div class="news-img">
									<img src="<?= get_template_directory_uri(); ?>/assets/images/news-img.png" alt="" />
								</div>
								<div class="news-text">
									<p>Bij een CVA wordt de stroom van het bloed naar een deel van de hersenen verstoord. Hierdoor krijgt het hersenweefsel te weinig zuurstof en raakt het beschadigd. Dat deel kan niet meer functioneren. Dit verlies kan tijdelijk of blijvend zijn. Direct na een...</p>
									<a href="#">Less Verder</a>
								</div>
							</li>
						</ul>
					</div>
				</div>
				<div class="right-bar">
					<div class="category-list">
						<ul>
							<li>
								<a href="<?php echo site_url() . '/about-us' ; ?>">Over ons</a>
							</li>
							<li>
								<a href="#">Missien</a>
							</li>
							<li>
								<a href="#">Activiteiten</a>
							</li>
							<li>
								<a href="#">Thema’s</a>
							</li>
							<li>
								<a href="<?php echo site_url() . '/about-us-team' ; ?>">Bestuursleden</a>
							</li>
						</ul>
					</div>
					<div class="about-side-div">
						<img src="<?= get_template_directory_uri(); ?>/assets/images/icon-quote.svg" alt="" />
						<p>Optional, room for a quote when there’s an interview with someone</p>
					</div>
					<div class="about-side-img">
						<img src="<?= get_template_directory_uri(); ?>/assets/images/about-sidebar-news-img.png" alt="" />
					</div>
				</div>
			</div>
		</div>
	</div>

<?php

get_footer();


?>