$('#sidebarCollapse').on('click', function () {
	$('#sidebar').toggleClass('active');
	$('.welcom-main').toggleClass('active');
});


var w = $(document).width();
if (w <= 991) {
    $('body').on('click', '#sidebarCollapse',function() {
	    $("#sidebar").show().animate({
	        right: "0px",
	    }, 500);
	});

	$(".cross-btn").click(function() {
	    $("#sidebar").animate({
	        right: "-310px",
	    }, 500);
	});
};

$('.search-icon-div a').click(function(e) {
    e.preventDefault();
    
    $('.search-div-sm').slideToggle(500,"swing");    
});

jQuery(document).ready(function($) {
	$(document).on('change','.filters', function(e) {
		e.preventDefault();
		$('.loading').fadeIn();
		var selected = "";
		$('input[name="agenda-filters"]:checked').each(function(e, val) {
		    selected += $(this).val()+",";
		    
		});
		
		var data = "action=agenda_filters&filter_value="+selected.substring(0, selected.length - 1)+"&filter_level="+$('.filters').attr('data-filter');
		$.post(ajaxurl, data, function(resp){
			// console.log(resp);
			resp = JSON.parse(resp);
			$('.agenda-con').html(resp.data);
			$('.loading').fadeOut();
		});
		
        
	});

	$(document).on('change','.project-filter', function(e) {
		e.preventDefault();
		$('.loading').fadeIn();
		if($('input[name="project_status"]:checked').val()) {
			if(window.location.href.includes('?')) { 
				var category = $('.project-cat').val();
				var data = "action=project_filters&filter_value="+$('input[name="project_status"]:checked').val()+"&category="+category;
			} else {
				var data = "action=project_filters&filter_value="+$('input[name="project_status"]:checked').val();
			}
			
			$.post(ajaxurl, data, function(resp){
				// console.log(resp);
				resp = JSON.parse(resp);
				$('.project-con').html(resp.data);
				$('.loading').fadeOut();
			});
		}
	});

	$(document).on('change','.project-cat', function(e) {
		if($.trim($(this).val()) != "") {
			if(window.location.href.includes('?')) {
				var url = window.location.href.split("?");
				window.location.href = url[0]+'/?category='+$(this).val();
			} else {
				window.location.href = window.location+'/?category='+$(this).val();
			}
			
		}
	});

});

$('.testDiv').slimscroll({
  height: '100%'
});