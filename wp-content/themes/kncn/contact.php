<?php


/*Template Name: contact_page*/



get_header();

if(isset($_GET['term'])) {
	$args = array(
        'post_type' => 'news',
        'post_status' => 'publish',
        'tax_query' => array(
            array(
                'taxonomy' => 'news_categories',
                'field'    => 'slug',
                'terms'    => @$_GET['term']
            )
        )
    );
} else {
	$args = array(
	    'post_type' => 'news',
	    'post_status'  => 'publish'
    );
}

$news = new WP_Query( $args );
$news = $news->posts;

?>


	<div class="news-page">
		<div class="wrap-1342">
			<div class="clearfix">
				<div class="news-left-section contact-div">
					<h2>De ondersteuning van het Kennisnetwerk vindt plaats vanuit het bureau, gecestigd in Maastricht.</h2>
					<div class="news-left-inr news-left-inr02 contact-inr">
						<ul>
							<li>
								<h3>Het bureau is bereikbaar op</h3>
								<p><span><img src="<?= get_template_directory_uri(); ?>/assets/images/phone-icon.png" alt="" /></span> 043-38 744 28</p>
								<p><span><img src="<?= get_template_directory_uri(); ?>/assets/images/mail-icon.png" alt="" /></span> <a href="#">info@kennisnetwrkcva.nl</a></p>
								<p class="text-small">(Indien geen gehoor, dan graag mailen)</p>
							</li>
							<li>
								<h3>Mederwerkers bureau Kennisnetwerk CVA NL</h3>
								<p>Helene Vogdt, drecteur</p>
								<p>Liesbeth van Hoef, webmaster/prjectmedewerer/contactpersoon Knowledge</p>
								<p>Brokers kb@kennisnetwerkcva.nl</p>
								<p>Moniq ue Bergsma, organisatorisch manager</p>
								<p>Henk-Jan Westendrop, bureaumedeweker</p>
								<p class="text-small">(U kunt eventueel een bericht achter  laten via het bijgaande contactformulier)</p>
							</li>
							<li>
								<h3>Postadres</h3>
								<p><span><img src="<?= get_template_directory_uri(); ?>/assets/images/phone-icon.png" alt="" /></span> <span class="address-text">Kennisnetwerk CVA NL</span> <span class="address-text address-text01">p/a: MUMC+, RVA Patient en Zorg, Postbus 5800, 6202 AZ Maastricht</span></p>
								<p><span><img src="<?= get_template_directory_uri(); ?>/assets/images/mail-icon.png" alt="" /></span> <a href="#">info@kennisnetwrkcva.nl</a></p>
								<p><span><img src="<?= get_template_directory_uri(); ?>/assets/images/phone-icon.png" alt="" /></span> 043-38 744 28</p>
								<p><span><img src="<?= get_template_directory_uri(); ?>/assets/images/card-icon.png" alt="" /></span> Bankrekningnummer: IBAN, NL 19RABOo129519642</p>
							</li>
						</ul>
						<div class="map-div">
							<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2450.6134645069533!2d5.111623315267772!3d52.10496597465668!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c66f3e37a8e0c5%3A0x7eee8911254bcbd7!2sOudlaan+4%2C+3515+GA+Utrecht%2C+Netherlands!5e0!3m2!1sen!2s!4v1527837186070" frameborder="0" style="border:0" allowfullscreen></iframe>
						</div>
					</div>
				</div>
				<div class="right-bar">
					<div class="news-brief-div contact-bar">
						<h3>CONTACT US</h3>
						<?php
                             echo do_shortcode(
                               '[contact-form-7 id="196" title="Contact Form"]'
                             );
                        ?>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php

get_footer();


?>