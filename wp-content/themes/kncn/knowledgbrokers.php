<?php


/*Template Name: knowledgebrokers_page*/

get_header();


?>


	<div class="news-page prod_det">
		<div class="wrap-1342">
			<div class="clearfix">
				<ul class="breadcrumb">
					<li class="breadcrumb-item"><a href="<?php echo site_url() . '/home' ; ?>">Home</a></li>
					<li class="breadcrumb-item"><a href="#">Kennisbank</a></li></li>
					<li class="breadcrumb-item active">Knowledge brokers netwerk</li>
				</ul>
				<div class="news-left-section">
					<h2 class="main-heading">Knowledge brokers netwerk</h2>
					<div class="news-left-inr project-detail-div">
						<div class="news-left-section news-detail-pg">
							<div class="detail-inr">
								<p>This is a generic page within the Kennisbank. For logged in knowledge an extra menu item will appear in the right hand menu.</p>   
								<p>Dolor ons adipi scing elite mauris urna adipi scing elite mauris urna tincidunt quia. Dolor ons adipi scing elite mauris urna adipi mauris urna tincidunt quia. dolor ons adipi scing elite mauris urna tincidunt quia. Dolor ons adipi scing elite mauris urna adipi mauris urna tinscing elite mauris urna adipi scing elite mauris urna tincidunt quia. Ipsum dolor ons adipi scing elite mauris urna adipi mauris urna tincidunt quia.</p>
								<p>Ipsum dolor ons adipi scing elite mauris urna adipi scing elite mauris urna tincidunt quia. Dolor ons adipi scing elite mauris urna adipi mauris urna tincidunt quia. dolor ons adipi scing elite maer uuris urna adipi scing elite mauris urna tincidunt quia. Ipsum dolor ons adipi scing elite mghi ioauris urna adipi mauris urna tincidunt quia.</p>
								<p>Dolor ons adipi scing elite mauris urna adipi scing elite mauris urna tincidunt quia. Dolor ons adipi scing elite mauris urna adipi mauris urna tincidunt quia. dolor ons adipi scing elite mauris urna tincidunt quia. Dolor ons adipi scing elite mauris urna adipi mauris urna tinscing elite mauris urna adipi scing elite mauris urna tincidunt quia. Ipsum dolor ons adipi scing elite mauris urna adipi mauris urna tincidunt quia.</p>
							</div>
						</div>
					</div>
				</div>
				<div class="right-bar quality-rightbar">
					<div class="category-list">
						<ul>
							<li>
								<a href="#">Keten</a>
							</li>
							<li>
								<a href="#">Projecten</a>
							</li>
							<li>
								<a href="#">Brochures</a>
							</li>
							<li>
								<a href="#">Publicaties</a>
							</li>
							<li class="active">
						    	<a class="nav-link collapsed" href="#submenu1" data-toggle="collapse" data-target="#submenu1">Knowledge brokers netwerk</a>
			                    <div class="collapse in" id="submenu1" aria-expanded="true">
			                        <ul class="flex-column pl-2 nav">
			                            <li class="nav-item"><a class="nav-link py-0" href="#">Item 1</a></li>
			                            <li class="nav-item">
			                                <a class="nav-link py-1" href="#">Item 2</a>
			                            </li>
			                            <li class="nav-item">
			                                <a class="nav-link py-1" href="#">Item 3</a>
			                            </li>
			                            <li class="nav-item">
			                                <a class="nav-link py-1" href="#">Item 4</a>
			                            </li>
			                            <li class="nav-item">
			                                <a class="nav-link py-1" href="#">Item 5</a>
			                            </li>
			                        </ul>
			                    </div>
						    </li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php

get_footer();


?>