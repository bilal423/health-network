<?php


/*Template Name: agenda_page*/

get_header();
$args = array(
    'post_type' => 'agenda',
    'post_status'  => 'publish',
    'orderby' => 'agenda_date',
    'order'   => 'ASC',
    'posts_per_page' => '-1',
    'meta_query'  => array(
    	array(
    		'key' => 'agenda_date',
    		'value' => date('Ymd'),
    		'compare' => '>=',
    		'type' => 'DATE'
    	),
    )
);

$agenda = new WP_Query( $args );

?>


	

<div class="news-page">
<div class="wrap-1342">
	<div class="clearfix">
		<ul class="breadcrumb">
			<li class="breadcrumb-item"><a href="<?php echo site_url() . '/home' ; ?>">Home</a></li>
			<li class="breadcrumb-item active">Agenda</li>
		</ul>
		<div class="news-left-section ">
			<h2 class="main-heading">Agenda</h2>
			<div class="news-left-inr">
				<ul class="agenda-con">
					<?php
                        if(sizeof($agenda->posts) > 0 ) { foreach($agenda->posts as $k => $ad) {
                    ?>
					<li>
						<a href="<?php echo site_url() . '/agenda-detail-page?postId=' . $ad->ID ; ?>">
							<div class="date-div <?= getAgendaSupportedClass(@$ad->ID);?>">
								<p><?= getAgendaDate($ad->ID); ?> <span><?= getAgendaMonth($ad->ID); ?></span></p>
							</div>
							<div class="calender-text">
								<h3><?= @$ad->post_title;?></h3>
								<p><?= @$ad->post_excerpt;?></p>
							</div>
						</a>

					</li>
					<?php } } else {  ?>
						<div class="news_post">
                            <h3>Geen agenda gevonden.</h3>
                        </div>
                    <?php } ?>
				</ul>
			</div>
		</div>
		<div class="right-bar">
			<div class="news-brief-div agenda-sidbar">
				<h3>Filter</h3>
				<ul>
					<li>
						<input class="filters" type="checkbox" id="c1" name="agenda-filters" value="Ketencoordinatoren" data-filter="agenda"/>
						<label for="c1" ><span></span>Ketencoordinatoren</label>
					</li>
					<li>
						<input class="filters" type="checkbox" id="c2" name="agenda-filters" value="Knowledgebrokers" data-filter="agenda"/>
						<label for="c2" ><span></span>Knowledgebrokers</label>
					</li>
					<li>
						<input class="filters" type="checkbox" id="c3" name="agenda-filters" value="KNCN algemeen" data-filter="agenda"/>
						<label for="c3" ><span></span>KNCN algemeen</label>
					</li>
					<li>
						<input class="filters" type="checkbox" id="c4" name="agenda-filters" value="Extern" data-filter="agenda"/>
						<label for="c4" ><span></span>Extern</label>
					</li>
					
				</ul>
				<p>Bijeenkomsten georganiseerd door KNCN</p>
				<p class="border-color1">Bijeenkomsten georganiseerd door ketens</p>
				<p class="border-color2">Georganiseerd door externe partijen</p>
			</div>
			<a class="btn archief-btn" href="<?php echo site_url() . '/archive-agenda-posts' ; ?>">Archief</a>
		</div>
	</div>
</div>
</div>

<?php

get_footer();

?>