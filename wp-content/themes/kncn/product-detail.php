<?php


/*Template Name: product_detail_page*/

get_header();

$pdet = get_post(@$_GET['postId']);
// echo "<pre>";
// print_r($pdet);
// echo "</pre>";

?>


	<div class="news-page prod_det">
		<div class="wrap-1342">
			<div class="clearfix">
				<ul class="breadcrumb">
					<li class="breadcrumb-item"><a href="<?php echo site_url() . '/home' ; ?>">Home Ingelodg</a></li>
					<li class="breadcrumb-item"><a href="<?php echo site_url() . '/brochures' ; ?>">Producten</a></li>
					<li class="breadcrumb-item active"><?= mb_strimwidth(@$pdet->post_title, 0, 30, "...") ;?></li>
				</ul>
				<div class="news-left-section">
					<h2 class="main-heading"><?= @$pdet->post_title;?></h2>
					<div class="news-left-inr project-detail-div">
						<div class="news-detail-pg">
							<div class="detail-inr">
								<?= wpautop(@$pdet->post_content);?>
							</div>
							<div class="project-top prod_top">
								<div class="select-style">
									<select class="project-cat">
	                                	<option>Selecteer Een Thema</option>
	                                    <option value=""></option>
	                                </select>
	                            </div>
	                            <a class="btn download_btn" href="#">Download Aanmelden</a>
							</div>
						</div>
					</div>
				</div>
				<div class="right-bar quality-rightbar">
					<div class="category-list">
						<ul>
							<li>
						    	<a class="nav-link collapsed" href="#submenu1" data-toggle="collapse" data-target="#submenu1">Ketens</a>
			                    <div class="collapse" id="submenu1" aria-expanded="false">
			                        <ul class="flex-column pl-2 nav">
			                            <li class="nav-item"><a class="nav-link py-0" href="#">Jaarplannen</a></li>
			                            <li class="nav-item">
			                                <a class="nav-link py-1" href="#">Borging en sturing</a>
			                            </li>
			                            <li class="nav-item">
			                                <a class="nav-link py-1" href="<?php echo site_url() . '/quality-standards' ; ?>">Kwaliteitsstandaarden</a>
			                            </li>
			                            <li class="nav-item">
			                                <a class="nav-link py-1" href="#">Scholing & Training</a>
			                            </li>
			                            <li class="nav-item">
			                                <a class="nav-link py-1" href="#">Projectmatig Werken</a>
			                            </li>
			                        </ul>
			                    </div>
						    </li>
							<li class="active">
								<a href="#">Projecten</a>
							</li>
							<li>
								<a href="#">Brochures</a>
							</li>
							<li>
								<a href="#">Wetenschappelijk onderzoek</a>
							</li>
							<li>
						    	<a href="#">Reports</a>
						    </li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php

get_footer();


?>