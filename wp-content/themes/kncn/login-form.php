<?php


/*Template Name: login_page*/

get_header();


?>


	<div class="news-page prod_det">
		<div class="wrap-1342">
			<div class="clearfix">
				<div class="news-left-section login-page">
					<div class="broker-div member-div login-div">
						<div class="login-inr">
							<h3>Inloggen</h3>
							<input type="email" name="email" class="form-control" placeholder="Gebruikersnaam">
							<input type="password" name="password" class="form-control form-control02" placeholder="Wachtwoord">
							<a class="btn" href="#">Inloggen</a>
							<a class="forget-paw" href="#">Wachtwoord vergeten?</a>
						</div>
						<div class="login-text">
							<p>Deze website is voor zorgprofessionals. Is de instelling waar u werkt aangrsloten bij het kennisnetwerk CVA, dan Kunt u een account aanvragen..</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php

get_footer();


?>