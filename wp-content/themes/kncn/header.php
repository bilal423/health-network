<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */



$menu = wp_get_menu_array('Main Menu');


?>
<!DOCTYPE html>
	<html <?php language_attributes(); ?> lang="en">
	<head>
		<meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	    <meta name="description" content="">
	    <meta name="author" content="">
	    <title><?php wp_title('Health Network -  '); ?></title>
	    <link href="<?= get_template_directory_uri(); ?>/assets/css/bootstrap.css" rel="stylesheet">
        

	    <?php wp_head(); ?>

        <!-- Custom styles for this template -->
        <link href="<?= get_template_directory_uri(); ?>/assets/css/reset.css" rel="stylesheet">
        <link href="<?= get_template_directory_uri(); ?>/assets/css/fonts.css" rel="stylesheet">
        <link href="<?= get_template_directory_uri(); ?>/assets/font-awesome/css/font-awesome.css" rel="stylesheet">
        <link href="<?= get_template_directory_uri(); ?>/assets/css/style.css" rel="stylesheet">
        <link href="<?= get_template_directory_uri(); ?>/assets/css/custom.css" rel="stylesheet">
        <script src="<?= get_template_directory_uri(); ?>/assets/js/jquery.min.js"></script>
        <script type="text/javascript">
	       var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
	   </script>


	</head>

	<body <?php body_class(); ?>>

		<div id="page" class="site">
			<div class="loading"><img src="<?=get_template_directory_uri()?>/assets/images/loading.gif"></div>
			<header id="masthead" class="site-header" role="banner">
				<div class="header-main">
					<div class="wrap-1342">
						<div class="header-inr">
							<div class="row">
								<div class="col-md-4 col-lg-4">
									<div class="clearfix">
										<div class="logo-div">
											<a href="<?php echo site_url() . '/home' ; ?>">
												<img src="<?= get_template_directory_uri(); ?>/assets/images/logo.svg" alt="" />
												<span class="hidden-sm-down">
													<p>Kennisnetwerk</p>
													<p class="colr-blue">CVA Nederland</p>
												</span>
											</a>
										</div>
										
										<div class="navbar-header hidden-md-up">
					                        <button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn">
					                            <img src="<?= get_template_directory_uri(); ?>/assets/images/navbar-icon.png" alt="" />
					                        </button>
					                    </div>

					                    <div class="search-icon-div hidden-md-up">
											<a href="#"><img class="" src="<?= get_template_directory_uri(); ?>/assets/images/search-icon-sm.svg" alt="" /></a>
										</div>
										<div class="search-div search-div-sm hidden-md-up">
	    									<input type="text" class="form-control" placeholder="Wat zoekt u?">
	    									<input class="search-icon" type="button" name="" value="" />
										</div>

					                </div>
								</div>
								<div class="col-md-8 col-lg-8">
									<div class="vind-div-main">
										<div class="search-div hidden-sm-down">
	    									<input type="text" class="form-control" placeholder="Wat zoekt u?">
	    									<input class="search-icon" type="button" name="" value="" />
										</div>
										<div class="navbar-header navbar-header01">
					                        <button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn">
					                            <img src="<?= get_template_directory_uri(); ?>/assets/images/navbar-icon.png" alt="" />
					                        </button>
					                    </div>
										<div class="vind-div">
											<ul>
												<li>
													<a href="#">
														<img src="<?= get_template_directory_uri(); ?>/assets/images/nederland.svg" alt="" />
														<p>Vind <span>ZORG</span></p>
													</a>
												</li>
												<li>
													<a href="#">
														<img src="<?= get_template_directory_uri(); ?>/assets/images/icon-forum.svg" alt="" />
														<p>Forum</p>
													</a>
												</li>
												<li>
													<a href="#">
														<img src="<?= get_template_directory_uri(); ?>/assets/images/icon-quality.svg" alt="" />
														<p>Kwaliteits <span>standaard</span></p>
													</a>
												</li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="navigation clearfix">
						<div class="wrap-1342">
							<nav class="navbar hidden-md-down">
								<ul class="navbar-nav">
									<?php foreach($menu as $key => $m ) :?>
										<li class="nav-item">
											<a class="nav-link <?php if (is_page($m['title'])) echo 'active'; ?>" href="<?=$m['url']; ?>"><?= $m['title']; ?></a>
											<?php if(sizeof($m['children']) > 0 ) : ?>
												<ul class="sub">
													<?php foreach($m['children'] as $key => $c) : ?>
														<li><a href="<?= $c['url']; ?>"><?= $c['title']; ?></a></li>
													<?php endforeach ?>
												</ul>
											<?php endif ?>	
										</li>	
									<?php endforeach?>	
								</ul>
							</nav>
							<div class="nav-btns">
								<ul>
									<li>
										<a class="btn btn-purpul" href="#">patiënten</a>
									</li>
									<li>
										<a class="btn" href="<?php echo site_url() . '/inloggen' ; ?>"><img src="<?= get_template_directory_uri(); ?>/assets/images/login-icon.svg" alt="" /> inloggen</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="sidebar hidden-lg-up" id="sidebar">
	                <span class="cross-btn hidden-lg-up">
	                    <a href="#">
	                    	<img src="<?= get_template_directory_uri(); ?>/assets/images/cross-btn.png" alt="" /> 
	                    </a>
	                </span>
	                <ul class="nav nav-sidebar testDiv">
	                   <?php foreach($menu as $key => $m ) :?>
							<li class="<?php if (is_page($m['title'])) echo 'active'; ?>">
								
								<?php if(sizeof($m['children']) > 0 ) { ?>
									<a class="dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?= $m['title']; ?><img src="<?= get_template_directory_uri(); ?>/assets/images/dropdown-arrow-sm.png" alt="" /></a>
									<div class="dropdown-menu" aria-labelledby="navbarDropdown">
										<?php foreach($m['children'] as $key => $c) : ?>
											<a class="dropdown-item" href="<?= $c['url']; ?>"><?= $c['title']; ?></a>
										<?php endforeach ?>
									</div>
								<?php } else { ?>
									<a href="<?=$m['url']; ?>"><?= $m['title']; ?></a>
								<?php } ?>	
							</li>	
						<?php endforeach?>	
	                </ul>
	            </div>
			</header><!-- #masthead -->

			<?php

			/*
			 * If a regular post or page, and not the front page, show the featured image.
			 * Using get_queried_object_id() here since the $post global may not be set before a call to the_post().
			 */
			if ( ( is_single() || ( is_page() && ! twentyseventeen_is_frontpage() ) ) && has_post_thumbnail( get_queried_object_id() ) ) :
				echo '<div class="single-featured-image-header">';
				echo get_the_post_thumbnail( get_queried_object_id(), 'twentyseventeen-featured-image' );
				echo '</div><!-- .single-featured-image-header -->';
			endif;
			?>

			<div class="site-content-contain">
				<div id="content" class="site-content">
