<?php


/*Template Name: product_page*/

get_header();

$args = array(
    'post_type' => 'producten',
    'post_status'  => 'publish'
);

$producten = new WP_Query( $args );

// echo "<pre>";
// print_r($producten->post);
// echo "</pre>";

?>


	<div class="news-page product_pg">
		<div class="wrap-1342">
			<div class="clearfix">
				<ul class="breadcrumb">
					<li class="breadcrumb-item"><a href="<?php echo site_url() . '/home' ; ?>">Home</a></li>
					<li class="breadcrumb-item active">Producten</li>
				</ul>
				<div class="news-left-section ">
					<h2 class="main-heading">Brochures Bestellen</h2>
					<div class="news-left-inr prod_inr">
						<ul>
							<?php
							if(sizeof($producten->posts) > 0) { foreach($producten->posts as $key => $pro) {
								
							?>
							<li>
								<div class="project-data-div prod_div clearfix">
									<h4><a href="<?php echo site_url() . '/brochures-details/?postId=' . $pro->ID ; ?>"><?= @$pro->post_title; ?></a></h4>
									<div class="project-text project-text02">
										<?php if (has_post_thumbnail( $pro->ID ) ): ?>
	                                        <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $pro->ID ), 'full' ); ?>
										<img src="<?php echo @$image[0]; ?>" alt="" />
										<?php endif; ?>
									</div>
									<a class="btn download_btn" href="<?php echo site_url() . '/brochures-details/?postId=' . $pro->ID ; ?>">Download Aanmelden</a>
								</div>
							</li>
							<?php } } else { ?>
								<div class="news_post">
                                    <h3>Geen producten gevonden.</h3>
                                </div>
							<?php } ?>
						</ul>
					</div>
				</div>
				<div class="right-bar quality-rightbar">
					<div class="category-list">
						<ul>
							<li>
						    	<a class="nav-link collapsed" href="#submenu1" data-toggle="collapse" data-target="#submenu1">Ketens</a>
			                    <div class="collapse" id="submenu1" aria-expanded="false">
			                        <ul class="flex-column pl-2 nav">
			                            <li class="nav-item"><a class="nav-link py-0" href="#">Jaarplannen</a></li>
			                            <li class="nav-item">
			                                <a class="nav-link py-1" href="#">Borging en sturing</a>
			                            </li>
			                            <li class="nav-item">
			                                <a class="nav-link py-1 <?php if (is_page('quality-standards')) echo 'active'; ?>" href="<?php echo site_url() . '/quality-standards' ; ?>">Kwaliteitsstandaarden</a>
			                            </li>
			                            <li class="nav-item">
			                                <a class="nav-link py-1" href="#">Scholing & Training</a>
			                            </li>
			                            <li class="nav-item">
			                                <a class="nav-link py-1" href="#">Projectmatig Werken</a>
			                            </li>
			                        </ul>
			                    </div>
						    </li>
							<li>
								<a href="#">Projecten</a>
							</li>
							<li class="<?php if (is_page('brochures')) echo 'active'; ?>">
								<a href="<?php echo site_url() . '/brochures' ; ?>">Brochures</a>
							</li>
							<li>
								<a href="#">Wetenschappelijk onderzoek</a>
							</li>
							<li>
						    	<a href="#">Reports</a>
						    </li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php

get_footer();


?>