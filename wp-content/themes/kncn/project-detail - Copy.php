<?php


/*Template Name: project_detail_page*/

get_header();

$pdetail = get_post(@$_GET['postId']);

?>


	<div class="news-page">
		<div class="wrap-1342">
			<div class="clearfix">
				<ul class="breadcrumb">
					<li class="breadcrumb-item"><a href="<?php echo site_url() . '/home' ; ?>">Home Ingelodg</a></li>
					<li class="breadcrumb-item"><a href="#">Groepen & Ketens</a></li>
					<li class="breadcrumb-item"><a href="<?php echo site_url() . '/projects' ; ?>">Projecten</a></li>
					<li class="breadcrumb-item active"><?= mb_strimwidth(@$pdetail->post_title, 0, 30, "..."); ?></li>
				</ul>
				<div class="news-left-section">
					<h2 class="main-heading main-heading02"><?= @$pdetail->post_title;?></h2>
					<span class="hding-date"><?= date('d-m-Y',strtotime(@$pdetail->post_date)); ?></span>
					<div class="news-left-inr project-detail-div">
						<div class="news-left-section news-detail-pg">
							<div class="detail-inr">
								<p><?= @$pdetail->post_content;?></p>
								<div class="next-btns next-btns02">
									<ul>
										<li>
											<a class="blue-btn" href="#"><img src="<?= get_template_directory_uri(); ?>/assets/images/left-arrow.png" alt="" /> Vorige</a>
										</li>
										<li>
											<a class="blue-btn" href="#">Volgende <img src="<?= get_template_directory_uri(); ?>/assets/images/right-arrow.png" alt="" /></a>
										</li>
									</ul>
									<a class="citizn" href="#">Terug naar overzicht</a>
								</div>
							</div>
						</div>
						<h2 class="scnd-hding">Anderen Bekeken ook</h2>
						<ul>
							<li class="clearfix">
								<div class="project-data-div">
									<h3>Patienteninfo</h3>
									<h4>Informatieavond Mantelzorgres</h4>
									<span>12-12-2017</span>
									<div class="project-img">
										<img src="<?= get_template_directory_uri(); ?>/assets/images/project-img01.png" alt="" />
									</div>
									<div class="project-text">
										<h4>Doel</h4>
										<p>Ipsum dolor ons tye adipi scing elwerite mauris urna adierpi.</p>
										<h4>Contact persoon</h4>
										<a href="#">Meer Info</a>
									</div>
								</div>
								<div class="project-data-div project-data-div01">
									<h3>Patienteninfo</h3>
									<h4>Informatieavond Mantelzorgres</h4>
									<span>12-12-2017</span>
									<div class="project-img">
										<img src="<?= get_template_directory_uri(); ?>/assets/images/project-img01.png" alt="" />
									</div>
									<div class="project-text">
										<h4>Doel</h4>
										<p>Ipsum dolor ons tye adipi scing elwerite mauris urna adierpi.</p>
										<h4>Contact persoon</h4>
										<a href="#">Meer Info</a>
									</div>
								</div>
							</li>
							<li class="clearfix">
								<div class="project-data-div">
									<h3>Patienteninfo</h3>
									<h4>Informatieavond Mantelzorgres</h4>
									<span>12-12-2017</span>
									<div class="project-img">
										<img src="<?= get_template_directory_uri(); ?>/assets/images/project-img01.png" alt="" />
									</div>
									<div class="project-text project-text01">
										<h4>Doel</h4>
										<p>Ipsum dolor ons tye adipi scing elwerite mauris urna adierpi.</p>
										<h4>Contact persoon</h4>
										<a href="#">Meer Info</a>
									</div>
								</div>
								<div class="project-data-div project-data-div01">
									<h3>Patienteninfo</h3>
									<h4>Informatieavond Mantelzorgres</h4>
									<span>12-12-2017</span>
									<div class="project-img">
										<img src="<?= get_template_directory_uri(); ?>/assets/images/project-img01.png" alt="" />
									</div>
									<div class="project-text">
										<h4>Doel</h4>
										<p>Ipsum dolor ons tye adipi scing elwerite mauris urna adierpi.</p>
										<h4>Contact persoon</h4>
										<a href="#">Meer Info</a>
									</div>
								</div>
							</li>
						</ul>
					</div>
				</div>
				<div class="right-bar">
					<div class="category-list">
						<ul>
							<li>
								<a href="#">Ketens</a>
							</li>
							<li class="active">
								<a href="#">Projecten</a>
							</li>
							<li>
								<a href="#">Brochures</a>
							</li>
							<li>
								<a href="#">Wetenschappelijk onderzoek</a>
							</li>
							<li>
								<a href="#">Publicaties</a>
							</li>
						</ul>
					</div>
					<div class="sidebar-logo-div">
						<?php $gallery = get_gallery( @$_GET['postId'] ); ?>

						<ul>
							<?php 
								if ($gallery) { foreach ($gallery as $key => $ga) {
							?>
							<li>
								<a href="#">
									<img src="<?php echo $ga->large_url ?>" alt="<?php echo $ga->alt ?>" />
								</a>
							</li>
							<?php } } ?>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php

get_footer();


?>