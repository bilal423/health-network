<?php


/*Template Name: archive-agenda*/

get_header();

$paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;

$args = array(
    'post_type' => 'agenda',
    'post_status'  => 'publish',
    'orderby' => 'agenda_date',
    'order' => 'DESC',
    'meta_query'  => array(
    	array(
    		'key' => 'agenda_date',
    		'value' => date('Ymd'),
    		'compare' => '<'
    	),
    ),
    'posts_per_page' => '10',
    'paged' => $paged

);

$agenda = new WP_Query( $args );
?>


	<div class="news-page">
		<div class="wrap-1342">
			<div class="clearfix">
				<ul class="breadcrumb">
					<li class="breadcrumb-item"><a href="<?php echo site_url() . '/home' ; ?>">Home</a></li>
					<li class="breadcrumb-item active"><a href="<?php echo site_url() . '/agenda' ; ?>">Agenda</a></li>
					<li class="breadcrumb-item active">Agenda archief</li>
				</ul>
				<div class="news-left-section ">
					<h2 class="main-heading">Agenda archief</h2>
					<div class="news-left-inr">
						<ul class="agenda-con">
							<?php
	                            if(sizeof($agenda->posts) > 0) { foreach($agenda->posts as $k => $ad) {
                            ?>
							<li>
								<a href="<?php echo site_url() . '/agenda-detail-page?postId=' . $ad->ID ; ?>">
									<div class="date-div <?= getAgendaSupportedClass(@$ad->ID);?>">
										<p><?= getAgendaDate($ad->ID); ?> <span><?= getAgendaMonth($ad->ID); ?></span></p>
									</div>
									<div class="calender-text">
										<h3><?= @$ad->post_title;?></h3>
										<p><?= @$ad->post_excerpt;?></p>
									</div>
								</a>
							</li>
							<?php } } else {  ?>
								<div class="news_post">
                                    <h3>Geen agenda gevonden.</h3>
                                </div>
                            <?php } ?>
						</ul>
					</div>
					<nav aria-label="Page navigation example">
						<ul class="pagination">
							<?php
								$big = 999999999; // need an unlikely integer

								echo paginate_links( array(
									'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
									'format' => '?paged=%#%',
									'current' => max( 1, get_query_var('paged') ),
									'total' => $agenda->max_num_pages
								) );
							?>
						</ul>
					</nav>
				</div>
				<div class="right-bar">
					<div class="news-brief-div agenda-sidbar">
						<h3>Filter</h3>
						<ul>
							<li>
								<input class="filters" type="checkbox" id="c1" name="agenda-filters" value="Ketencoordinatoren" data-filter="archive"/>
								<label for="c1" ><span></span>Ketencoordinatoren</label>
							</li>
							<li>
								<input class="filters" type="checkbox" id="c2" name="agenda-filters" value="Knowledgebrokers" data-filter="archive"/>
								<label for="c2" ><span></span>Knowledgebrokers</label>
							</li>
							<li>
								<input class="filters" type="checkbox" id="c3" name="agenda-filters" value="KNCN algemeen" data-filter="archive"/>
								<label for="c3" ><span></span>KNCN algemeen</label>
							</li>
							<li>
								<input class="filters" type="checkbox" id="c4" name="agenda-filters" value="Extern" data-filter="archive"/>
								<label for="c4" ><span></span>Extern</label>
							</li>
								
						</ul>
						<p>Bijeenkomsten georganiseerd door KNCN</p>
						<p class="border-color1">Bijeenkomsten georganiseerd door ketens</p>
						<p class="border-color2">Georganiseerd door externe partijen</p>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php

get_footer();


?>