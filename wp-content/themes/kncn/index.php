<?php


/*Template Name: index_page*/



get_header();


?>


	<div class="main-page">
		<div class="wrap-1342">
			<div class="main-inner">
				<?php if(get_post_meta('186','page_title')[0]) : ?>
					<h1><?= get_post_meta('186','page_title')[0]?></h1>
				<?php endif?>
				<div class="white-div clearfix">
					<?php  get_template_part('template-parts/home/content', 'home');?>
				</div>
			</div>
		</div>
		<div class="gray-div">
			<div class="wrap-1342">
				<div class="home-sections clearfix">
					<?php  get_template_part('template-parts/home/content', 'news');?>
					<?php  get_template_part('template-parts/home/content', 'agenda');?>
					<?php  get_template_part('template-parts/home/content', 'twitter');?>
				</div>
			</div>
		</div>
		<div class="">
			<?php // fetchTweets( array( 'id' => '201' ) ); ?>
		</div>
	</div>

<?php

get_footer();


?>