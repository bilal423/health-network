<?php


/*Template Name: quality_standards_detail_page*/

get_header();

$pdetail = get_post(@$_GET['postId']);
// echo "<pre>";
// print_r($pdetail);
// echo "</pre>";

?>


	<div class="news-page">
		<div class="wrap-1342">
			<div class="clearfix">
				<ul class="breadcrumb">
					<li class="breadcrumb-item"><a href="<?php echo site_url() . '/home' ; ?>">Home Ingelodg</a></li>
					<li class="breadcrumb-item"><a href="#">Groepen & Ketens</a></li>
					<li class="breadcrumb-item"><a href="<?php echo site_url() . '/quality-standards' ; ?>">Kwaliteitsstandaarden</a></li>
					<li class="breadcrumb-item active"><?= mb_strimwidth(@$pdetail->post_title, 0, 30, "...") ;?></li>
				</ul>
				<div class="news-left-section">
					<h2 class="main-heading"><?= @$pdetail->post_title;?></h2>
					<div class="news-left-inr project-detail-div">
						<div class="news-left-section news-detail-pg">
							<div class="detail-inr">
								<?= wpautop(@$pdetail->post_content);?>
							</div>
						</div>
					</div>
				</div>
				<div class="right-bar quality-rightbar">
					<div class="category-list">
						<ul>
							<li>
						    	<a class="nav-link collapsed" href="#submenu1" data-toggle="collapse" data-target="#submenu1">Ketens</a>
			                    <div class="collapse" id="submenu1" aria-expanded="false">
			                        <ul class="flex-column pl-2 nav">
			                            <li class="nav-item"><a class="nav-link py-0" href="#">Jaarplannen</a></li>
			                            <li class="nav-item">
			                                <a class="nav-link py-1" href="#">Borging en sturing</a>
			                            </li>
			                            <li class="nav-item">
			                                <a class="nav-link py-1" href="<?php echo site_url() . '/quality-standards' ; ?>">Kwaliteitsstandaarden</a>
			                            </li>
			                            <li class="nav-item">
			                                <a class="nav-link py-1" href="#">Scholing & Training</a>
			                            </li>
			                            <li class="nav-item">
			                                <a class="nav-link py-1" href="#">Projectmatig Werken</a>
			                            </li>
			                        </ul>
			                    </div>
						    </li>
							<li class="active">
								<a href="#">Projecten</a>
							</li>
							<li>
								<a href="#">Brochures</a>
							</li>
							<li>
								<a href="#">Wetenschappelijk onderzoek</a>
							</li>
							<li>
						    	<a href="#">Reports</a>
						    </li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php

get_footer();


?>