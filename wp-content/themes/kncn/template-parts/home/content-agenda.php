<?php

/**
 * Template part for displaying speaker posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

$agenda_args = array(
    'post_type' => 'agenda',
    'post_status'  => 'publish',
    'posts_per_page' => '3',
    'orderby' => 'agenda_date',
    'order'   => 'ASC',
    'meta_query'  => array(
    	array(
    		'key' => 'agenda_date',
    		'value' => date('Ymd'),
    		'compare' => '>=',
    		'type' => 'DATE'
    	),
    )
);

$agenda = new WP_Query( $agenda_args );
?>

<div class="home-agenda-div">
	<h4 class="home-section-hding">Agenda</h4>
	<div class="news-brief-div single-agenda-bar bar-home">
		<div class="news-left-inr">
			<ul>
				<?php if(sizeof($agenda->posts) > 0) { foreach($agenda->posts as $key => $agenda) { ?>
				<li>
					<div class="date-div <?= getAgendaSupportedClass(@$agenda->ID);?>">
						<p><?= getAgendaDate($agenda->ID); ?> <span><?= getAgendaMonth($agenda->ID); ?></span></p>
					</div>
					<div class="calender-text">
						<p><?= mb_strimwidth(@$agenda->post_excerpt, 0, 70, "..."); ?></p></p>
					</div>
				</li>
				<?php } } ?>
			</ul>
			<span class="cel-link"><a href="<?= site_url().'/agenda'?>">Alle bijeenkomsten</a></span>
		</div>
	</div>
</div>