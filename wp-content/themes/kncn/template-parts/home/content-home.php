<?php

/**
 * Template part for displaying speaker posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */



?>

<ul>
	
	<?php if(get_post_meta('186','post_title_1')[0]) : ?>
		<li>
			<img src="<?php echo wp_get_attachment_url( get_post_meta('186','post_image_1')[0] );?>" alt="" />
			<h3><a href="<?php echo get_post_meta('186','post_url_1')[0];?>"><?php echo get_post_meta('186','post_title_1')[0];?></a></h3>
			<p><?= mb_strimwidth(get_post_meta('186','post_content_1')[0], 0, 175, "..."); ?></p>
			<a href="<?php echo get_post_meta('186','post_url_1')[0];?>">lees verder</a>
		</li>
	<?php endif ?>
	<?php if(get_post_meta('186','post_title_2')[0]) : ?>
		<li>
			<img src="<?php echo wp_get_attachment_url( get_post_meta('186','post_image_2')[0] );?>" alt="" />
			<h3><a href="<?php echo get_post_meta('186','post_url_2')[0];?>"><?php echo get_post_meta('186','post_title_2')[0];?></a></h3>
			<p><?= mb_strimwidth(get_post_meta('186','post_content_2')[0], 0, 175, "..."); ?></p>
			<a href="<?php echo get_post_meta('186','post_url_2')[0];?>">lees verder</a>
		</li>
	<?php endif ?>

	<?php if(get_post_meta('186','post_title_3')[0]) : ?>
		<li>
			<img src="<?php echo wp_get_attachment_url( get_post_meta('186','post_image_3')[0] );?>" alt="" />
			<h3><a href="<?php echo get_post_meta('186','post_url_3')[0];?>"><?php echo get_post_meta('186','post_title_3')[0];?></a></h3>
			<p><?= mb_strimwidth(get_post_meta('186','post_content_3')[0], 0, 175, "..."); ?></p>
			<a href="<?php echo get_post_meta('186','post_url_3')[0];?>">lees verder</a>
		</li>
	<?php endif ?>	
	
</ul>
<?php if(get_post_meta('186','post_title_4')[0]) : ?>
<div class="broker-div">
	<img src="<?php echo wp_get_attachment_url( get_post_meta('186','post_image_4')[0] );?>" alt="" />
	<h3><?php echo get_post_meta('186','post_title_4')[0];?></h3>
	<a href="<?php echo get_post_meta('186','post_url_4')[0];?>">
		<img src="<?= get_template_directory_uri(); ?>/assets/images/blue-circle-arrow.svg" alt="" />
	</a>
</div>
<?php endif ?>	