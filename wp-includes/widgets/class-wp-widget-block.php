<?php
/**
 * Widget API: Widget_Block class
 *
 * @package WordPress
 * @subpackage Widgets
 * @since 4.4.0
 */

/**
 * Core class used to implement a Block widget.
 *
 * @since 2.8.0
 *
 * @see WP_Widget
 */
class Widget_Block extends WP_Widget {

	public function __construct() {
		$widget_ops = array(
			'classname' => 'widget_block',
			'description' => __( 'A search form for your site bilal.' ),
			'customize_selective_refresh' => true,
		);
		parent::__construct( 'block', _x( 'Block', 'Search widget Bilal' ), $widget_ops );
		add_action( 'widgets_init', function(){
			register_widget( 'Widget_Block' );
		});
	}
	
}


// function myplugin_register_widgets() {
// 	register_widget( 'Widget_Block' );
// }

// add_action( 'widgets_init', 'myplugin_register_widgets' );
// $my_widget = new Widget_Block();